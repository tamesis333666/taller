<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-perfil-{{ Auth::user()->id}}">
	{{Form::Open(array('action'=>array('ApExpenseRevenueController@destroy',$rset->id ),'method'=>'post'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Cancelar Registro</h4>
			</div>
			<div class="modal-body">
				<p>Confirme si desea cancelar el registro seleccionado</p>
				<p>Esta accion es irreversible</p>
				<p>Empleado:  <b> {{ $rset->empleado->FULL_NAME }}     </b></p> 
				<p>Monto:  <b> {{ $rset->amount  }}     </b></p> 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>