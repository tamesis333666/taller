<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Siscad</title>
</head>
<body>
<p>Estimado {{$name}} </p> 
<p>Este es un correo informativo </p>
<p>{{$messagenote}} </p>
 

	<table class="table table-hover display table-responsive table-condensed" border=1 	id="table">
		<thead>
		<tr background-color: #EBF8FC;>
			<th width=25%>VEHICULO</th>
			<th width=25%>DOCUMENTO</th>                                
			<th width=25%>FECHA EXPIRACION</th>
			<th width=25% ># DIASXVENCER</th>
			<th width=25% >MENSAJE</th>
		</tr>
		</thead>
		<tbody>
		@foreach ($infomail as $cat)
			<tr>
				<td>{{ $cat->placa}}</td>
				<td>{{ $cat->documento}}</td>
				<td align="right"> {{ date('d-m-Y', strtotime($cat->date_to)) }} </td>
				 
				<td align="right">{{ $cat->dias_por_vencer}}</td>		
				<td>
					@if ( $cat->dias_por_vencer<0 )
						"GESTIONAR RENOVACIÓN"
					@elseif ( $cat->dias_por_vencer>=0 &&  $cat->dias_por_vencer<30  )
						"PRÓXIMO A VENCER"
					@else
						"A TIEMPO"
					@endif
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
<br>						
<div class="footer">
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="content-block">
                <span class="apple-link">Siscad Inc</span>

            </td>
        </tr>
        <tr>

        </tr>
    </table>
</div>
</body>
</html>
