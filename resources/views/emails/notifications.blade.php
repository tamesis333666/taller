@extends('layouts.admin')

@section('title', 'Listado de vehiculos')

@section('contenido')
    <div class="box">
        @include('asesor.vehiculo.partials.success')
        <div class="box-header with-border">
            <h3 class="box-title">
                Listado de Notificaciones
            </h3>
            
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover display table-responsive table-condensed" id="table">
                            <thead>
                            <tr>
                                <th>message_type</th>
                                <th>message_name</th>
                                <th>recipient_role</th>
                                <th>status</th>
                                <th>mail_status</th>
                                <th>begin_date</th>
                                <th>from_user</th>
                                <th>recipient_role</th>
                                <th>to_user</th>
                                <th>subject</th>                                 
								<th>created_at</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($listado as $lista)
                                <tr>
                                    <td>   {{ $lista->message_type }}   </td>
									<td>   {{ $lista->message_name }}   </td>
									<td>   {{ $lista->recipient_role }}   </td>
									<td>   {{ $lista->status }}   </td>
									<td>   {{ $lista->mail_status }}   </td>
									<td>   {{ $lista->begin_date }}   </td>
									<td>   {{ $lista->from_user }}   </td>
									<td>   {{ $lista->recipient_role }}   </td>
									<td>   {{ $lista->to_user }}   </td>
									<td>   {{ $lista->subject }}   </td>    
									<td>   {{ $lista->created_at }}   </td>        									
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <!-- footer-->
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#table').DataTable({
                "language": {
                    "url": "{{ asset('AdminLTE/plugins/datatables/esp.lang') }}"
                }
            });
        });
    </script>
@endsection