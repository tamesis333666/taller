<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                
                <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <!--Contenido-->
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Datos del Usuario</h3>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('name', 'Nombre usuario') !!}
                                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                            {!! Form::label('effective_end_date', 'Fecha Hasta') !!}
                                            {!! Form::date('effective_end_date', (isset($rssite) && $rssite->effective_end_date ? \Carbon\Carbon::parse($rssite->effective_end_date)->format('Y-m-d') :''), ['class' => 'form-control', 'placeholder' => '', 'required'], 'd/m/Y') !!}
                                        </div>
                                    <div class="form-group">
                                        <div class="col-xl-6">
                                                {!! Form::label('person_id', 'Cliente') !!}

                                        </div>
                                       
                                        {!! Form::select('person_id', $client,  array('class' => 'form-control')) !!}
                                    </div>
                                   
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('email', 'Correo') !!}
                                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('password', 'Contraseña') !!}
                                        {!! Form::password('password', ['class' => 'form-control', 'required']) !!}
                                       
                                    </div>
                                     
                                    <div class="form-group">
                                        {!! Form::label('type', 'Tipo') !!}
                                        
                                        {!! Form::select('type', ['Interno' => 'Interno', 'Externo' => 'Externo'], 'Externo', array('class' => 'form-control')) !!}
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>