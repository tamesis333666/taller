@extends('layouts.admin')

@section('title', 'Registro de usuarios web')

@section('contenido')
    {!! Form::open(['route' => 'usuario.web.store', 'class' => 'form', 'method' => 'POST', 'id' => 'form']) !!}
    @include('webfront.partials.fields')
    <div class="for text-center">
        {!! Form::submit('Registrar', ['class'=> 'btn btn-primary']) !!}
        <a class="btn btn-danger" href="{{ route('usuario.web.index')}}">
            Cancelar
        </a>
    </div>
    {!! Form::close() !!}
@endsection

@section('js')
<script>
    $('#person_id').select2();
</script>
@endsection
 