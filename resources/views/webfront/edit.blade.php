@extends('layouts.admin')

@section('title', 'Editar Acceso Usuario')

@section('contenido')
    {!! Form::model($rssite, ['route' => ['usuario.web.update', $rssite],  'method' => 'PUT']) !!}
    
    @include('webfront.partials.fields')

    <div class="for text-center">
        {!! Form::submit('Editar', ['class'=> 'btn btn-primary']) !!}


        
        <a class="btn btn-danger" href="{{ route('usuario.web.index')}}">
            Cancelar
        </a>
    </div>

    {!! Form::close() !!}
@endsection

 @section('js')
 <script>
  $('#person_id').select2();
 </script>
 
@endsection