@extends('layouts.admin')

@section('title', 'Listado de usuarios Web')

@section('contenido')
    <div class="box">
        @include('webfront.partials.success')
        <div class="box-header with-border">
            <h3 class="box-title">
                Listado de usuarios Web
            </h3>
            <div class="box-tools">
                <div class="text-center">
                     
                    <a class="btn btn-danger btn-sm" href="{{ route('usuario.web.create') }}">
                        NUEVO REGISTRO
                    </a>
                     
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover display table-responsive table-condensed" id="table">
                            <thead>
                            <tr>
                                <th>USUARIO</th>
                                <th>TIPO</th>
                                <th>EMAIL</th>
                                <th>FECHA CADUCIDAD</th>
								 
                                <th>ACCIONES</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rssite as $var)
                                <tr>
                                    <td> {{ $var->name }} </td>
                                    <td> {{ $var->type }} </td>
									<td> {{ $var->email }} </td>
									<td> {{ $var->effective_end_date }} </td>
									 
                                     
                                    <td>
                                         
                                        <a href="{{URL::action('UserWebController@edit',$var->id)}}">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <!-- footer-->
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#table').DataTable({
                "language": {
                    "url": "{{ asset('AdminLTE/plugins/datatables/esp.lang') }}"
                }
            });
        });
    </script>
@endsection