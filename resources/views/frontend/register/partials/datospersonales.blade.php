<div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Datos personales</h3>
        </div>

        <div class="form-group">
            {!! Form::label('date_of_birth', 'Fecha de Nacimiento') !!}
            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                {!! Form::text('date_of_birth', null, ['class' => 'form-control datepicker', 'placeholder' => '' ]) !!}
            </div>
        </div>

        <div class="form-group">
            {{ Form::radio('sex', 'M', false) }} Hombre
            <br>
            {{ Form::radio('sex', 'F', false) }} Mujer
        </div>

		<div class="form-group">
    		<div class="form-group">
    			<label>Documento</label>
    			<select name="tipo_documento" class="form-control">
                       <option value="0">DNI</option>
                       <option value="1">RUC</option>
                       <option value="2">PAS</option>
    			</select>
    		</div>
    	</div>
        <div class="form-group">
            {!! Form::label('num_documento', 'Número de Documento') !!}
            {!! Form::text('num_documento', null, ['class' => 'form-control', 'placeholder' => '' ]) !!}
        </div>
    </div>
</div>