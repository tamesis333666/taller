<div class="col-md-12">
    <div class="box box-primary">
	
	
		<div class="form-group row">
			<label class="col-sm-4 col-form-label-sm"align=right >*  Documento</label>
			<div class="col-sm-4">
				<div>
					<select name="tipo_documento" class="form-control" required=true	>
						   <option value="0">DNI</option>
						   <option value="1">RUC</option>
						   <option value="2">CE</option>
						   <option value="3">PASAPORTE</option>
					</select>
 				</div>
			</div>		
 														
	 
		
		</div> 
		
		<div class="form-group row">
			<label class="col-sm-4 col-form-label-sm"align=right >Numero</label>
			<div class="col-sm-8">
				<div>
					{!! Form::text('first_name',  old('first_name'), ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
				</div>
			</div>		
		</div> 
		
        <div class="form-group row">
			<label class="col-sm-4 col-form-label-sm"align=right >*   Razon Social</label>
			<div class="col-sm-8">
				<div>
					{!! Form::text('first_name',  old('first_name'), ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
				</div>
			</div>																
		</div> 
		
		<div class="form-group row">
			<label class="col-sm-4 col-form-label-sm"align=right >*   Apellido Paterno</label>
			<div class="col-sm-8">
				<div>
					{!! Form::text('first_last_name', null, ['class' => 'form-control', 'placeholder' => ''  ]) !!}
				</div>
			</div>																
		</div> 
		
		<div class="form-group row">
			<label class="col-sm-4 col-form-label-sm"align=right >*   Apellido Materno</label>
			<div class="col-sm-8">
				<div>
					{!! Form::text('second_last_name', null, ['class' => 'form-control', 'placeholder' => '' ]) !!}
				</div>
			</div>																
		</div> 
		
		<div class="form-group row">
			<label class="col-sm-4 col-form-label-sm"align=right >*   Correo</label>
			<div class="col-sm-8">
				<div>
				{!! Form::email('email_address', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
				</div>
			</div>																
		</div> 
		
		<div class="form-group row">
			<label class="col-sm-4 col-form-label-sm"align=right >*    Celular</label>
			<div class="col-sm-8">
				<div>
					{!! Form::text('telef2', null, ['class' => 'form-control inputmask2', 'placeholder' => '']) !!}
				</div>
			</div>																
		</div> 
		 
		 
		{{Form::hidden('create_method','autocust')}}
    </div>
</div>