@extends('layouts.app')

@section('content')
<style>
    body {
        font-size: 14px !important;
        font-family: Arial, Helvetica, sans-serif;
        margin: 0px 0px 10px 0px;
        padding: 0px;
        background: #FFF;
        color: #666666;
    }

   
    .MainArea {
        margin-left: auto;
        margin-right: auto;
        padding-left: 20px;
        padding-right: 20px;
        display: block;
        width: 955px;
    }
 
    .left {
        width: 475px;
        height: 475px;
        padding: 0px 0px 10px 0px;
        float: left;
        display: block;
    }
 
    .right {
        width: 320px;
        float: right;
        display: block;
        background: #FFF;
    }
 
	    .copyright_login {
        float: right;
        font-size: 12px;
        color: #702789;
        padding: 4px 0px 0px 0px;
    }
</style>

<div class="container" id="app">
	<div class="row">
		<div id="Div1" class="MainArea">
			<div class="left">
				<div class="left_img">
					<img src="/imagenes/login/logo_right.jpg" alt="">
				</div>
			</div>	
				<div class="right">
					 
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        
                        <div class="box-header with-border">
							@include('ventas.partials.errors')
							<h3 class="box-title">Nuevo Cliente</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">
									<!--Contenido-->
									@include('frontend.register.partials.fields')
									 
								</div>
								
								</div>
						</div>
 
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i> Acceder
                                </button>
 
                            </div>
							 
                        </div>
						
						 
                    </form>
				</div>

			 
		</div>
		<div class="foot_login">
            <div class="foot_login_content">
                
                <div class="copyright_login">
                    Copyright Asvnets 2019. All rights reserved.
                </div>
            </div>
        </div>
		
	</div>
 
</div>
@endsection

@section('js')
    <script type="text/javascript" src="{{asset('js/sites.js')}}"></script>
@endsection