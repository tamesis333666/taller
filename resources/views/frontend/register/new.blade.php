@extends('layouts.app')
 
@section('content')
<style>
    body {
        font-size: 14px !important;
        font-family: Arial, Helvetica, sans-serif;
        margin: 0px 0px 10px 0px;
        padding: 0px;
        background: #FFF;
        color: #666666;
    }

   
    .MainArea {
        margin-left: auto;
        margin-right: auto;
        
        padding-right: 80px;
        display: block;
        width: 1055px;
    }
 
    .left {
        width: 475px;
        height: 475px;
        padding: 0px 0px 10px 0px;
        float: left;
        display: block;
    }
 
    .right {
        width: 420px;
        float: right;
        display: block;
        background: #FFF;
    }
 
	    .copyright_login {
        float: right;
        font-size: 12px;
        color: #702789;
        padding: 4px 0px 0px 0px;
    }
</style>

   {!! Form::open(['route' => 'ventas.cliente.store', 'method' => 'POST']) !!}
	<div class="row">
		<div id="Div1" class="MainArea">
			<div class="left">
				<div class="left_img">
					<img src="/imagenes/frontend/register/logo_right.jpg" alt="">
				</div>
			</div>	
			
			<div class="right">
				
					<!--Contenido-->
					@include('frontend.register.partials.fields')
				
			</div>
			
			<div class="for text-center">
				{!! Form::submit('Registrar', ['class'=> 'btn btn-primary']) !!}
				<a class="btn btn-danger" href="{{ route('ventas.cliente.index')}}">
					Cancelar22
				</a>
			</div>
				 
		</div>
	</div>
     
     {!! Form::close() !!}

@push ('scripts')
<script>
     
</script>
@endpush

@endsection

@section('js')
    <script>
        $(".inputmask1").inputmask("(999) 9999999");
        $(".inputmask2").inputmask("(999) 999999999");  

        $('.datepicker').datepicker({
            format: "dd-mm-yyyy",
            language: "es",
            autoclose: true
        });
         
    //Para activacion de textarea mediante checkbox
        function showContent() {
            element = document.getElementById("content");
            check = document.getElementById("check");
            if (check.checked) {
                element.style.display = 'block';
            }
            else {
                element.style.display = 'none';
            }
        }

    </script>
@endsection
