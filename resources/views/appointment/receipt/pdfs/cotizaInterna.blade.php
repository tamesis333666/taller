<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cotizacion N° {{ $d->nrocotizac }} - PDF</title>
    @include('pdfs.includes.style')
</head>
<body>
         
    
	<header id="header">
		<div  class="row">
			<table WIDTH=100% >
				<tr>
					<td class="tg-yw4l" width="50%" align="center">
						<img src="{{ $d->sucursal->path_image_logo }}"  alt="Logo" class="img-responsive center-block" width="200px">
					</td>
					<td class="tg-yw4l" colspan="3" width="50%" align="center">
                        <h4>Fecha:{{ $d->created_at }}</h4>
						<h3>COTIZACION - INTERNA</h3>
						<br>
						Nro Cotizacion: <b>{{ $d->nrocotizac }}</b>
					</td>
					<td class="tg-yw4l" width="50%" align="center">
						 <img src="{{ asset('imagenes/logo/logo-bosh.jpg') }}"  alt="Logo" class="img-responsive center-block" width="100px"> 					 
					</td>
				</tr>
			</table>
		</div>
    </header>
        @include('pdfs.includes.header-client')


        <div class="row">
            <table WIDTH=100% >
                <tr>
                    <th  colspan="4">Respuesto de Lista</th>
                </tr>
                <tr>
                    <td width=70%>Descripcion</td>
                    <td width=10%>Precio</td>
                    <td width=10%>Cantidad</td>
                    <td width=10%>Total</td>
                </tr>
                @foreach($d->repuestos as $repuesto)
                <tr>
                    <td>@if( $repuesto->descripcion == $repuesto->old_description )
						{{ $repuesto->descripcion }}
						@else
						**{{ $repuesto->descripcion }}	
						@endif
					</td> 
                    <td align="right">{{ number_format( $repuesto->precio, 2 )   }}</td>
                    <td align="right">{{ $repuesto->cantidad }}</td>
                    <td align="right">{{ number_format ( $repuesto->precio  * $repuesto->cantidad ,2) }}</td>
                </tr>
                @endforeach
                <tr>
                    <th colspan="3" class="total">Total</th>
                    <th class="total">{{ number_format ($d->totalLinea->SUM('total'), 2 ) }}</th>
                </tr>
            </table>
        </div>
		
		<div class="row">
           <table WIDTH=100% >
                <tr>
                    <th  colspan="4">Repuesto Manuales</th>
                </tr>
                <tr>
                    <td width=70%>Descripcion</td>
                    <td width=10%>Precio</td>
                    <td width=10%>Cantidad</td>
                    <td width=10%>Total</td>
                    
                </tr>

                @foreach($d->repuestos_manual as $repuesto)
                <tr>
                    <td>{{ $repuesto->repuesto }}</td>
                    <td align="right">{{ number_format( $repuesto->costo , 2 )  }}</td>
                    <td align="right">{{ $repuesto->cantidad }}</td>
                    <td class="total" align="right">{{ number_format ( $repuesto->costo * $repuesto->cantidad ,2) }} </td>
                </tr>
                @endforeach
                <tr>
                    <th class="total"></th>
                    <th class="total"></th>
                    <th class="total">Total</th>
                    <th class="total">{{ number_format ($d->totalLineaMan->SUM('total'), 2 ) }}</th>
                </tr>

            </table>
        </div>

		
        <div class="row">
            <table class="full-table">
                <tr>
                    <th  colspan="2">Mano de Obra</th>
                </tr>
                <tr>
                    <td>Descripcion</td>
                    <td width="100px">Total</td>
                </tr>
                @foreach($d->mano_obra as $mano_obra)
                <tr>
					 <td align="left">{{$mano_obra->descripcion }}</td>	
                    <td align="right">{{ number_format( $mano_obra->costolabor  , 2 ) }}</td>
                </tr>
                @endforeach
                 
                <tr>
                    <th class="total">Total</th>
                    <th class="total">{{ number_format ($d->totalLineaLab->SUM('total'), 2 ) }}</th>
                </tr>

            </table>
        </div>

        

        <div class="row">
            <table class="full-table">
                <tr>
                    <th  colspan="2">Mano de Obra Manuales</th>
                </tr>
                <tr>
                    <td>Descripcion</td>
                    <td width="100px">Total</td>
                </tr>
                
                @foreach($d->mano_obra_manual as $mano_obra)
                <tr>
                    <td>{{ $mano_obra->manoobra }}</td>
                    <td align="right">{{ number_format( $mano_obra->costo , 2 )  }}</td>
                </tr>
                @endforeach
                <tr>
                    <th class="total">Total</th>
                    <th class="total">{{ number_format ($d->totalLineaLabMan->SUM('total'), 2 )  }}</th>
                </tr>
            </table>
        </div>
     
	<div class="row">
		<table>
			<tr>
				<th rowspan="3">
					- Esta cotizacion esta sujeta a variaciones si al realizar el servicio se requieren repuestos o trabajos adicionales.
					- Esta cotizacion esta realizada segun requerimiento, terminos de referencia o especificaciones tecnicas
					- Esta cotizacion tiene vigencia de 30 dias habiles a partir de su creacion
				</th>
				<th width="100px">
					Sub Total (S/)
				</th>
				<th width="100px" style="text-align:right">{{ number_format( ($d->totalLinea->SUM('total') + $d->totalLineaMan->SUM('total') + 
					$d->totalLineaLab->SUM('total') + $d->totalLineaLabMan->SUM('total') )/1.18 ,2) 
                    }}</th> 
			</tr>
			<tr>
				<th>I.G.V (18%)</th>
				<th style="text-align:right">{{  number_format( ($d->totalLinea->SUM('total') + $d->totalLineaMan->SUM('total') + 
					$d->totalLineaLab->SUM('total') + $d->totalLineaLabMan->SUM('total') )-($d->totalLinea->SUM('total') + $d->totalLineaMan->SUM('total') + 
					$d->totalLineaLab->SUM('total') + $d->totalLineaLabMan->SUM('total') )/1.18 ,2) }}</th>
			</tr>
			<tr>
				<th>TOTAL (S/.)</th>
				<th style="text-align:right"> {{ number_format( $d->totalLinea->SUM('total') + $d->totalLineaMan->SUM('total') + 
					$d->totalLineaLab->SUM('total') + $d->totalLineaLabMan->SUM('total') ,2) 
                    }} </th>
			</tr>
		</table>
	</div>


<footer id="footer">
    <table class="border-none" width="100%">
        <tr class="border-none">
            <td class="border-none border-left" width="100%">
                EUC: {{ $d->sucursal->num_1099 }} <br>
                 {{ $d->sucursal->address }} <br>
                 {{ $d->sucursal->telef }}  <br>
                 {{ $d->sucursal->email }}                  
            </td>
            <td class="border-none">
                Visto Bueno _______________________
                <br>
                
                 {{ $d->asesor->FULL_NAME }} <br>
                 Movil: {{ $d->asesor->TELEF2 }} 
            </td>
        </tr>
    </table>
</footer>

</body>
</html>