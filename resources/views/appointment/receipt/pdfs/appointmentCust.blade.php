<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Nro Cita N° {{ $d->numcita }} - PDF</title>
    @include('pdfs.includes.style')
	
	<style>
		 
		#watermark {
			position: fixed;
 
			bottom:   7cm;
			left:     2cm;

			/** Change image dimensions**/
			width:    15cm;
			height:   15cm;

			/** Your watermark should be behind every content**/
			z-index:  -1000;
		}
	</style>
	
</head>
<body>
         
     
	<header id="header">
		<div  class="row">
			<table WIDTH=100% >
				<tr>
					<td class="tg-yw4l" width="50%" align="center">
						<img src="{{ $d->sucursal->path_image_logo }}"  alt="Logo" class="img-responsive center-block" width="200px">
					</td>
					<td class="tg-yw4l" colspan="3" width="50%" align="center">
                        <h4>Fecha:{{ $d->created_at }}</h4>
						<h3>RESERVA CITA - CLIENTE</h3>
						
							 {{ $d->sucursal->name }}
						<br>
						Nro CITA: <b>{{ $d->numcita }}</b>
					</td>
					<td class="tg-yw4l" width="50%" align="center">
						 <img src="{{ asset('imagenes/logo/logo-bosh.jpg') }}"  alt="Logo" class="img-responsive center-block" width="100px"> 					 
					</td>
				</tr>
			</table>
		</div>
    </header>
    
	<br>
	<div class="row">
		<table class="full-table">
			<tbody>
				<tr>
					<th width="13%">DNI / RUC</th>
					<td>{{ $d->cliente->num_documento }}</td>

					<th width="13%">PLACA</th>
					<td>{{ $d->vehiculo->placa }}</td>
				</tr>

				<tr>
					<th width="13%">CLIENTE</th>
					<td>{{ $d->cliente->full_name }}</td>
					
					<th width="13%">MARCA</th>
					<td>{{ $d->vehiculo->marca->nombre }}</td>
				</tr>
				
				<tr>
					<th width="13%">DIRECCION</th>
					<td>{{ $d->cliente->address }}</td>
					
					<th width="13%">MODELO</th>
					<td>{{ $d->vehiculo->modelo->nombre }}</td>
				</tr>
				
				<tr>
					<th width="13%">CORREO</th>
					<td>{{ $d->cliente->email_address }}</td>

					<th width="13%">FABRICACION</th>
					<td>{{ $d->vehiculo->año }}</td>
				</tr>
				
				<tr>
					<th width="13%">TELEFONO</th>
					<td>{{ $d->cliente->telef1 }}</td>

					<th width="13%">  </th>
					<td>  </td>
				</tr>
			</tbody>
		</table>
	</div>


        <div class="row">
            <table WIDTH=100% >
                <tr>
                    <th  >Servicios</th>
                </tr>
                <tr>
                     
					<td width=70%>Servicio</td>
                    <td width=30%>Fecha Cita</td>
                    
                </tr>
				  
                 <tr>
				 
                    <td> {{ substr(  $d->servicio1->nombrelabor ,0,65 ) }}	 </td> 
                    <td align="right">{{ $d->datetime_reservation }}</td>
                </tr> 
                <tr>
				 
                    <td> {{ substr(  $d->servicio2->nombrelabor  ,0,65 ) }}	 </td> 
                    <td align="right">{{ $d->datetime_reservation }}</td>
                </tr>
                 
            </table>
        </div>
		 
         
	 
	<div class="row">
			OBSERVACIONES
            <table class="full-table">
                <tr> 
					<td   width="100%">{{ $d->comments }} </td>
				  
                </tr>
                  
            </table>
        </div>



<footer id="footer">
    <table class="border-none" width="100%">
        <tr class="border-none">
            <td class="border-none border-left" width="100%">
                RUC: {{ $d->sucursal->num_1099 }} <br>
                 {{ $d->sucursal->address }} <br>
                 {{ $d->sucursal->telef }}  <br>
                 {{ $d->sucursal->email }}                  
            </td>
            <td class="border-none">
                Visto Bueno _______________________
				<br>
				
 				 
            </td>
        </tr>
    </table>
</footer>

</body>
</html>