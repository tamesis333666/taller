@extends('layouts.admin')
@section('title', 'Registro de Servicios')
@section('contenido')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">                   
				<h3 class="box-title">Nueva Cita de Servicio</h3>
			</div>
		  
			{!!Form::open(array('url'=>'appointment/servicio/save','method'=>'POST','autocomplete'=>'off' , 'enctype' => 'multipart/form-data','files'=>'true'))!!}
			{{Form::token()}}
			<input type="hidden" name="action" value="Nuevo">
			<div class="box-body">
				<div class="nav-tabs-custom"> 
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#tab_1" data-toggle="tab">INF. GENERAL</a>
						</li>
						<!--
						<li>
							<a href="#tab_2" data-toggle="tab">INF. COMPLEMENTARIA</a>
						</li>
						<li>
							<a href="#tab_3" data-toggle="tab">DATOS DE APROBACION</a>
						</li>
						
						<li>
							<a href="#tab_4" data-toggle="tab">ADJUNTOS</a>
						</li>	
					-->						
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">								 
							<div class="row">
								<div class="col-md-12">									 
									<!--Contenido-->
									@include('appointment.register.partials.fields')									  
								</div> 									
							</div>								 
						</div>
						 
						<!-- inicio de adjuntos-->

						<div class="tab-pane fade" id="tab_4" >

							<div class="row">
				 
								<div class="col-md-4">
									<div class="form-group">

										{!! Form::label('description_adjunto', 'DESCRIPCION') !!}

										{!! Form::textarea('description_adjunto', null, ['class' => 'form-control', 'size' => '30x2', 'placeholder' => '']) !!}

									</div>

									<div class="form-group">
										{!! Form::label('file', 'Agregar Adjunto') !!}
										{!! Form::file('file[]',null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
										<button class="add_more">Agregar otro Archivo</button>
									</div>
									@if ($errors->has('file'))
									<span style="color:red">{{ $errors->first('file') }}</span>
									@endif
								</div>
								<div class="col-md-8">
									@if ($operacion == 'Create')
										Grabe la informacion antes de adjuntar archivos
									@else
									  
										@foreach($purchaseOrder->attachedDocuments as $doc)
											<div class="col-md-2">
												<div class="">
												<a href="{{ url('documents/'.$doc->name_file) }}" download>
													<i style="font-size: 6em;" class="fa fa-file-text-o"></i>
												</a>
												</div>
												<div class="" style="height: 50px">
												{{ $doc->name_file }}
												</div>
												<div class="">
												<a class="btn btn-danger" href="{{ url('orden/eliminar/documento/'.$doc->attached_document_id) }}">
													Eliminar
												</a>
												</div>
											</div>
										@endforeach
									@endif
								</div>
				 

							</div>


						</div>

						<!-- FIN de adjuntos-->					 
					</div>
				</div>
			</div>
			<div class="box-footer pull-right">
				{!! Form::submit('Registrar', ['class'=> 'btn btn-primary']) !!}
				<a class="btn btn-danger" href="">
					Cancelar
				</a>
			</div>
			{!!Form::close()!!}	
		</div>
	</div>
</div>	
@endsection

@section('js')
<script>
	//SElECT CON FILTRO
        $(document).ready(function() {
        $(".select2").select2();
        });

        //datepicker
        $('.datepicker').datepicker({
            format: "dd-mm-yyyy",
            language: "es",
            autoclose: true
        });
		
		$(window).load(function(){
			// PROCESO PARA CARGAR VISTA PREVIA DE IMAGE SUCURSAL
			 $(function() {
					  $('#path_image_logo').change(function(e) {
					      addImage(e); 
					     });

				     function addImage(e){
				      var file = e.target.files[0],
				      imageType = /image.*/;
				    
				      if (!file.type.match(imageType))
				       return;
				  
				      var reader = new FileReader();
				      reader.onload = fileOnload;
				      reader.readAsDataURL(file);
				     }
			  
				     function fileOnload(e) {
				      var result=e.target.result;
				      $('#imgSalida').attr("src",result);
				     }

			    });
	  	});

</script>
@endsection
 
