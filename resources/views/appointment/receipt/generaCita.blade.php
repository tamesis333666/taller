@extends('layouts.admin')

@section('title', 'Listado de Sucursal')

@section('contenido')
   
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <div class="box">
   		
   	@foreach($citas as $cita)

 

   		<div  class="row">
          <div class="col-md-4">
          <div class="panel panel-success">
          <div class="panel-body">
            Numero cita
          </div>
          <div class="panel-footer">{{ $cita->num_cita }}</div>
        </div>
        </div>

   			<div class="col-md-4">
   				<div class="panel panel-success">
				  <div class="panel-body">
				    Cliente
				  </div>
				  <div class="panel-footer">{{ $cita->full_name }}</div>
				</div>
   			</div>

   			<div class="col-md-4">
   				<div class="panel panel-success">
				  <div class="panel-body">
				   Placa
				  </div>
				  <div class="panel-footer"> {{ $cita->placa }}</div>
				</div>
   			</div>
   				
		</div>
		   	@endforeach	
   	</div>	


   	<h1>Tomar foto con la camara</h1>
        <p id="errorTxt"></p>
        <div>
            <video id="theVideo" controls autoplay></video>
            <canvas id="theCanvas"></canvas>
        </div>
        <button id="btnCapture" class="btn btn-danger">Capture</button>
        <button id="btnDownloadImage" class="btn btn-link">Download captured image</button>
        
        
        
        
    	@foreach($citas as $cita)     
	<form method="post" action="{{ route('fotocar.subir',[$cita->num_cita,$cita->placa, $cita->idcliente]) }}" enctype="multipart/form-data">
		  <input type="hidden" name="_token" value="{{ csrf_token() }}">


	  <div style="padding: 10px;display: none;" id="fileappend">
      <button type="button" id="fileappendbtn">Agregar otro archivo</button>
    </div>

	  <div style="margin-top: 10px;">
      <button class="btn btn-success" type="submit">Subir Fotos</button>
    </div>
	</form> 

	@endforeach


  
   <script type="text/javascript">
	    var iterator = 0;
            var videoWidth = 320;
            var videoHeight = 240;
            var videoTag = document.getElementById('theVideo');
            var canvasTag = document.getElementById('theCanvas');
            var btnCapture = document.getElementById("btnCapture");
            var btnDownloadImage = document.getElementById("btnDownloadImage");
            videoTag.setAttribute('width', videoWidth);
            videoTag.setAttribute('height', videoHeight);
            canvasTag.setAttribute('width', videoWidth);
            canvasTag.setAttribute('height', videoHeight);
            window.onload = () => {
                navigator.mediaDevices.getUserMedia({
                    audio: true,
                    video: {
                      
                        width: videoWidth,
                        height: videoHeight
                    }
                }).then(stream => {
                    videoTag.srcObject = stream;
                }).catch(e => {
                    document.getElementById('errorTxt').innerHTML = 'ERROR: ' + e.toString();
                });
                var canvasContext = canvasTag.getContext('2d');
                btnCapture.addEventListener("click", () => {
                    canvasContext.drawImage(videoTag, 0, 0, videoWidth, videoHeight);
                    
                    setimage( canvasTag.toDataURL() );
                    
                });
                btnDownloadImage.addEventListener("click", () => {
                    var link = document.createElement('a');
                    link.download = 'capturedImage.png';
                    link.href = canvasTag.toDataURL();
                    link.click();
                });
            };
        </script>
  



	  <script>
	    var i = 0;
	    function setimage( base64img ){
		    var e = document.createElement('div');
		    e.innerHTML = '<div style="padding: 10px;"><label>Archivo '+(++i)
				  +': </label><input name="filesToUpload_'
				  +(i)+'" id="filesToUpload_'
				  +(i)+'" type="hidden" value="'
				  +base64img+'" /><img src="'+base64img+'" width="32" height="24" /></div>';
		    document.querySelector("#fileappend").before(e);
	    }
	    document.querySelector("#fileappendbtn")
		    .addEventListener("click", function (e){
		    
		    var e = document.createElement('div');
		    e.innerHTML = '<div style="padding: 10px;"><label>Archivo '+(++i)+': </label><input name="filesToUpload_'+(i)+'" id="filesToUpload_'+(i)+'" type="file" multiple="" /></div>';
		    document.querySelector("#fileappend").before(e);
	    });
	  </script>

	
   
@endsection

@section('js')

	
@endsection