@extends ('layouts.admin')

@section('titlepage',count($data_cotizacion)>0?'Editar Cotización':'Nueva Cotización: ') 


@section('contenido')

    {{ Form::open(['id' => 'frmCotizacion']) }}


        {{--CUERPO DEL FORMULARIO--}}
        <div class="box-body">
            {{--TABS PARA DATOS DE CLIENTE Y VEHICULO--}}
            id cliente
            {{--END TABS PARA DATOS DE CLIENTE Y VEHICULO--}}

            {{--TABS PARA TOTALES, REPUESTOS MANUALES Y MANO MANUAL--}}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tabb_1" data-toggle="tab">CREADOS</a>
                    </li>
                    <li>
                        <a href="#tabb_2" data-toggle="tab">MANUALES</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tabb_1">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="">Repuestos</label>
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input class="form-control" type="number" readonly id="total_repuestos" name="total_repuestos">
                                    <span class="input-group-btn"> <button class="btn btn-info btn-flat" type="button" data-toggle="modal" data-target=".bs-repuestos-modal-lg" id="btn-repuestos-modal-lg" {!! !isset($data_cotizacion['cotizacion']->totalrep)?'disabled':'' !!}> <i class="fa fa-plus"></i> </button> </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="">Mano de Obra</label>
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input class="form-control" type="number" id="total_mano_obra" name="total_mano_obra" readonly value="{!! $data_cotizacion['cotizacion']->totalmo or '' !!}">
                                    <span class="input-group-btn"> <button class="btn btn-info btn-flat" type="button" data-toggle="modal" data-target=".bs-manoObra-modal-lg" id="btn-manoObra-modal-lg" {!! isset($data_cotizacion['cotizacion']->totalmo)?'':'disabled' !!}> <i class="fa fa-plus"></i> </button> </span>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane" id="tabb_2">
                        <div class="row">
                            <div class="col-md-6">
                                {{--ENTRADAS MANUALES DE REPUESTOS--}}
                                <br>
                                <label for="">Listado Repuestos</label>
                                <br><br>
                                <div class="row">
                                    <div class="col-xs-10">
                                        <div class="row">
                                        
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label for="Descripcion">Descripcion</label>
                                                    <input type="text" placeholder="Descripción" class="form-control" id="input_desc_manual_repuestos" name="input_desc_manual_repuestos">
                                                </div>
                                                
                                            </div>
											<div class="col-xs-4">
                                                <div class="form-group">
                                                <label for="Orden">Orden</label>
                                                    <input type="number" min="1" placeholder="Orden" class="form-control" id="input_orden_manual_repuesto" name="input_orden_manual_repuesto">
                                                </div>
                                            </div>
                                            
                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="Precio">Precio</label>
                                                    <input type="number" min="1" placeholder="Precio" class="form-control" id="input_precio_manual_repuestos" name="input_precio_manual_repuestos">
                                                </div>
                                            </div>

                                            <div class="col-xs-3">
                                                 <div class="form-group">
                                                     <label for="Cantidad">Cantidad</label>
                                                    <input type="text" placeholder="Cantidad" maxlength="6"  class="form-control" id="input_cantidad_manual_respuesto" name="input_cantidad_manual_respuesto">
                                                </div>
                                            </div>
											 
                                        </div>
                                        

                                    </div>

                                           

                                    <div class="col-xs-2">
                                        <button class="btn btn-primary" type="button" id="btnAddManualRepuestos" disabled> > </button>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-xs-10">
                                        <div class="form-group">
                                            <select name="list_manual_repuestos" id="list_manual_repuestos" multiple class="form-control" style="min-height: 250px">
                                                @if(isset($data_cotizacion['manuales_repuestos']) && count($data_cotizacion['manuales_repuestos'])>0)
                                                    @foreach($data_cotizacion['manuales_repuestos'] as $man_repuesto_manual)
                                                        <option value="" data-manual-orden="{!! $man_repuesto_manual->orden !!}" data-manual-desc="{!! $man_repuesto_manual->repuesto !!}" data-manual-precio="{!! $man_repuesto_manual->costo !!}" data-manual-cant="{!! $man_repuesto_manual->cantidad !!}">{!! $man_repuesto_manual->orden !!}  -  {!! $man_repuesto_manual->repuesto !!} {!! $man_repuesto_manual->costo !!} {!! $man_repuesto_manual->cantidad !!}</option>'
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <button type="button" class="btn btn-danger" id="btnRemoveManualRepuestos" disabled> - </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" id="total_manuales_repuestos" name="total_manuales_repuestos" readonly="" placeholder="Total">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                {{--ENTRADAS MANUALES DE MANO DE OBRA--}}
                                <br>
                                <label for="">Listado Mano Obra</label>
                                <br><br>
                                <div class="row">
                                    <div class="col-xs-10">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                <label for="Descripcion">Descripcion</label>
                                                    <input type="text" placeholder="Descripción" class="form-control" id="input_desc_manual_mano_obra" name="input_desc_manual_mano_obra">
                                                </div>
                                            </div>

                                            

                                            <div class="col-xs-8">
                                                <div class="form-group">
                                                <label for="Precio">Precio</label>
                                                    <input type="number" min="1" placeholder="Precio" class="form-control" id="input_precio_manual_mano_obra" name="input_precio_manual_mano_obra">
                                                </div>
                                            </div>
											<div class="col-xs-4">
                                                <div class="form-group">
                                                <label for="Orden">Orden</label>
                                                    <input type="number" min="1" placeholder="Orden" class="form-control" id="input_orden_manual_mano_obra" name="input_orden_manual_mano_obra">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <button class="btn btn-primary" type="button" id="btnAddManualManoObra" disabled> > </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-10">
                                        <div class="form-group">
                                            <select name="list_manual_mano_obra" id="list_manual_mano_obra" multiple class="form-control" style="min-height: 250px">
                                                @if(isset($data_cotizacion['manuales_obras']) && count($data_cotizacion['manuales_obras'])>0)
                                                    @foreach($data_cotizacion['manuales_obras'] as $man_obra_manual)
                                                        <option value="" data-manual-orden="{!! $man_obra_manual->orden !!}" data-manual-desc="{!! $man_obra_manual->manoobra !!}" data-manual-precio="{!! $man_obra_manual->costo !!}">{!! $man_obra_manual->orden !!} - {!! $man_obra_manual->manoobra !!} {!! $man_obra_manual->costo !!}</option>'
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <button type="button" class="btn btn-danger" id="btnRemoveManualManoObra" disabled> - </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" id="total_manuales_mano_obra" name="total_manuales_repuestos" readonly placeholder="Total">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
            {{--END TABS PARA TOTALES, REPUESTOS MANUALES Y MANO MANUAL--}}

            {{--CATALOGO DE ENCARGADO--}}
            <div class="row" style="display: none;">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">Encargado</label>
                               <input type="text" value="{{ Auth::user()->person_id }}" id="cat_encargado" name="cat_encargado">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--END CATALOGO DE ENCARGADO--}}



            {{--OPCIONES DE SUBTOTAL--}}

                <div class="row">
            
                    <div class="col-md-12">
                        <label for="">Observaciones</label>
                        <div class="input-group">
                            <textarea name="observa" class="form-control" id="observa" cols="70" rows="4">{!! $data_cotizacion['cotizacion']->comments or '' !!}</textarea>
                        </div>
                    </div>



                    <div class="col-md-6">
                        <div class="form-group" >
                            <label for="tota_igv">SUBTOTAL</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" class="form-control" id="total_subtotal_cotizacion" name="total_subtotal_cotizacion" readonly>
                            </div>
                        </div>
                    </div>           
                </div>
            {{--END OPCIONES DE SUBTOTAL--}}

        <div class="row">
             {{--OPCIONES DE IGV--}}
            
                <div class="col-md-6">
                    <div class="form-group" >
                        <label for="igv">IGV</label>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" class="form-control" id="igv_t" name="igv_t" readonly>
                        </div>
                    </div>
                </div>
            </div>
            {{--END OPCIONES DE IGV--}}

            {{--OPCIONES DE DESCUENTO--}}
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group" >
                        <label for="total_descuento">DESCUENTO</label>
                        <div class="input-group">
                            <span class="input-group-addon">%</span>
                            <input type="number" class="form-control" id="total_descuento_cotizacion" name="total_descuento_cotizacion" min="0" max="10" step="1" value="{{ $data_cotizacion['cotizacion']->dscto or 0 }}">
                        </div>
                    </div>
                </div>
            


                <div class="col-md-4">
                    <div class="form-group" >
                        <label for="total_descuento">IMPORTE DSCT</label>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="number" class="form-control" id="importe_dsc" name="importe_dsc" readonly>
                        </div>
                    </div>
                </div>
            </div>

            {{--END OPCIONES DE SUBTOTAL--}}

            {{--IMPUESTO IGV--}}
            <div class="row" style="display: none;">
                <div class="col-md-6">
                    <div class="form-group" >
                        <label for="tota_igv">IGV</label>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" class="form-control" id="tota_igv" name="tota_igv" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" style="display: none;">
                    <div class="form-group" style="padding-top: 2em;">
                        <label>
                            <input type="checkbox" class="minimal" name="ivg1" id="ivg1" {!! isset($data_cotizacion['cotizacion']->igv) && $data_cotizacion['cotizacion']->igv>0?'checked':'' !!}>
                        </label>
                        IGV
                    </div>
                </div>
            </div>
            {{--END IMPUESTO IGV--}}

            {{--TOTAL DE LA COTIZACION--}}
            <div class="row">
                <div class="col-md-6">
                    <label for="tota_cotizacion">TOTAL COTIZACIÓN</label>
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" class="form-control" id="total_cotizacion" name="total_cotizacion" readonly>
                    </div>
                </div>
            </div>
            {{--END TOTAL DE LA COTIZACION--}}
        </div>
        {{--END CUERPO DEL FORMULARIO--}}

        {{--BOTONES DEL FORMULARIO--}}
        <div class="box-footer">
            <div class="row">
                <div class="col-md-6" style="margin: 1em 0em;">
                    <button class="btn btn-primary btn-block" type="button" id="btnSaveCotizacion">
                        GENERAR COTIZACIÓN
                    </button>
                </div>
                <div class="col-md-6" style="margin: 1em 0em;">
                    <a href="{!! url('/asesor/cotizacion') !!}" class="btn btn-danger btn-block">
                        Cancelar
                    </a>
                </div>
            </div>
        </div>
        {{--END BOTONES DEL FORMULARIO--}}

        {{--MODALES PARA EL FORMULARIO--}}
        <div class="modal fade bs-repuestos-modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title">Repuestos</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label>Artículo</label>
                                  
                                    <select class="form-control" style="width: 100%;" tabindex="-1" aria-hidden="true" id="cat_repuestos" name="cat_repuestos">
                                        <option value="">Selecciona ...</option>
                                        @if(isset($data_cotizacion['cat_repuestos']) && count($data_cotizacion['cat_repuestos'])>0)
                                            @foreach($data_cotizacion['cat_repuestos'] as $repue)
                                                <option value="{!! $repue->iditem !!}" data-iditem="{!! $repue->iditem !!}" data-codigo="{!! $repue->codigo !!}" data-d1="{!! $repue->nombre !!}" data-p1="{!! $repue->list_price_per_unit !!}"  data-precio="{!! $repue->list_price_per_unit !!}" data-list_price_per_unit="{!! !is_null($repue->list_price_per_unit)?$repue->list_price_per_unit:0 !!}" data-descripcion="{!! $repue->nombre !!}" data-modificar="{!! $repue->allow_change_price !!}" data-cant_sugerida="{!! $repue->cant_sugerida  == null?0:$repue->cant_sugerida!!}">{!! $repue->nombre !!} </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">

                                <button class="btn btn-block btn-primary" id="addRepuesto" type="button" style="margin-top: 24px;">
                                    Agregar
                                </button>
                            </div>
                        </div>
                        <table class="table table-bordered table-hover" id="table_repuestos">
                            <thead>
                                <tr>
                                    
                                    <th class="text-center">
                                        Descripción
                                    </th>
                                    <th class="text-center">
                                        Precio
                                    </th>
                                    <th class="text-center">
                                        Precio de Lista (Venta)
                                    </th>
                                    <th class="text-center">
                                        Cant. Sugerida
                                    </th>
                                    <th class="text-center">
                                        Cantidad
                                    </th>
                                    <th class="text-center">
                                        Total
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($data_cotizacion['repuestos']) && count($data_cotizacion['repuestos'])>0)
                                    @foreach($data_cotizacion['repuestos'] as $repuesto)
                                        <tr data-codigo="{!! $repuesto->codd !!}" data-d1="{!! $repuesto->old_description !!}" data-p1="{!! $repuesto->old_price !!}" data-iditem="{!! $repuesto->idrep !!}" data-descripcion="{!! $repuesto->descripcion !!}" data-precio="{!! $repuesto->precio !!}" data-cantidad="{!! $repuesto->cantidad !!}" data-subtotal="{!! $repuesto->subtotal !!}" data-cant_sugerida="{{ !is_null($repuesto->cant_sugerida)?$repuesto->cant_sugerida:0 }}" data-list_price_per_unit="{{ !is_null($repuesto->list_price_per_unit)?$repuesto->list_price_per_unit:0 }}" >
                                            
                                            <td style="width: 30%;">{!! $repuesto->descripcion !!}</td>
                                            <td>{!! $repuesto->precio !!}</td>
                                            <td>
                                            {{ $repuesto->costo }}
                                            </td>
                                            <td>
                                                {{ $repuesto->cantidad }}
                                            </td>
                                            <td>
                                                <input type="number" class="form-control repo_cantidad" value="{!! $repuesto->cantidad !!}">
                                            </td>
                                            <td>
                                                {!! $repuesto->subtotal !!}
                                            </td>
                                            <td style="text-align: center;">
                                                <i class="fa fa-remove icon_remove" style="color:red;margin-top: 11px;cursor: pointer;"></i>
                                                {{--@if($repuesto->allow_change_price)--}}
                                                @if($data_cotizacion['cat_repuestos'][0]->allow_change_price)
                                                    &nbsp;<i class="fa fa-pencil icon_edit" style="color:blue;margin-top: 11px;cursor: pointer;"></i>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="5" style="text-align: right;"> Total:</td>
                                    <td>&nbsp;</td>
                                    
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="save_repuestos">Guardar Repuestos</button>
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bs-manoObra-modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Mano de Obra</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Mano de Obra</label>
                                            <select class="form-control" style="width: 100%;" tabindex="-1" aria-hidden="true" id="cat_mano_obra" name="cat_mano_obra">
                                                <option value="">Selecciona ...</option>
                                                @if(isset($data_cotizacion['cat_mano_obra']) && count($data_cotizacion['cat_mano_obra'])>0)
                                                    @foreach($data_cotizacion['cat_mano_obra'] as $manoo)
                                                        <option value="{!! $manoo->idlabor !!}" data-descripcion="{!! $manoo->nombrelabor !!}" data-precio="{!! $manoo->subtotal !!}" data-iditem="{!! $manoo->idlabor !!}">{!! $manoo->nombrelabor !!}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="mano_obra_precio_ivg">Precio sin IVG</label>
                                            <input type="number" class="form-control" min="1" id="mano_obra_precio_ivg" name="mano_obra_precio_ivg" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6" >
                                        <div class="form-group">
                                            <label for="mano_obra_precio_moodif">Precio Modificado</label>
                                            <input type="number" class="form-control" min="1" id="mano_obra_precio_moodif" name="mano_obra_precio_moodif">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-block btn-primary" id="addManoObra" type="button" style="margin-top: 24px;">
                                    Agregar
                                </button>
                            </div>
                        </div>
                        <table class="table table-bordered table-hover" id="table_mano_obra">
                            <thead>
                                <tr>
                                    <th class="text-center">
                                        Descripción
                                    </th>
                                    <th class="text-center">
                                        Precio de Lista
                                    </th>

                                     <th class="text-center">
                                        Precio modificado
                                    </th>
                                   
                                   
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($data_cotizacion['mano_obra']) && count($data_cotizacion['mano_obra'])>0)
                                    @foreach($data_cotizacion['mano_obra'] as $manoobra)
                                        <tr data-iditem="{!! $manoobra->idlabor !!}" data-condicion="{!! $manoobra->condicion !!}" data-descripcion="{!! $manoobra->descripcion !!}" data-precio="{!! $manoobra->costolabor !!}"  data-precio-modificado="{!! $manoobra->costolabor !!}'">
                                            <td>{!! $manoobra->descripcion !!}</td>
                                           
                                            <td>{!! $manoobra->costoinit !!}</td>
                                            <td>
                                                <input type="number" class="form-control labor_cantidad" value="{!! $manoobra->costolabor !!}">
                                                
                                            </td>
                                            
                                            
                                            
                                            <td style="text-align: center;">
                                                <i class="fa fa-remove icon_remove" style="color:red;margin-top: 11px;cursor: pointer;"></i>
                                                
                                                
                                                     &nbsp;<i class="fa fa-pencil icon_edit_mano" style="color:blue;margin-top: 11px;cursor: pointer;"></i>
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2" style="text-align: right;"> Total:</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btnAddTableMano">Guardar Mano de Obra</button>
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- END MODALES PARA EL FORMULARIO--}}

    {!! Form::close() !!}

@endsection

@section('js')
    <script>
        $(document).ready(function() {

            get_total_cotizacion();

            /**
             * VARIABLES
             */
            var documento               = $(document),
                input_cliente           = $('#idcliente'),
                input_vehiculo          = $('#id_vehiculo'),
				input_tipo_vehiculo		= $('#id_use_vehicle'),
                input_encargado         = $('#cat_encargado'),
                input_descuento         = $('#total_descuento_cotizacion'),
                data_envio              = '',
                btnSaveCotizacion       = $('#btnSaveCotizacion'),
                btnAddRepuesto          = $('#addRepuesto'),
                btnAddManoObra          = $('#addManoObra'),
                cat_repuestos           = $('#cat_repuestos'),
                tbl_repuestos           = $('#table_repuestos'),
                cat_mano_obra           = $("#cat_mano_obra"),
                tbl_mano_obra           = $('#table_mano_obra'),
                list_manual_manoobra    = $('#list_manual_mano_obra'),
                list_manual_repuestos   = $('#list_manual_repuestos'),
                contenido               = '',
                item                    = '',
                row                     = '',
                index                   = '',
                total                   = '',
                observa                 = $('#observa');

            $('#idcliente,#id_vehiculo,#cat_repuestos,#cat_mano_obra').select2();

            /**
             * ACCIONES PARA DESCUENTO
             */
            input_descuento.on('change',get_total_cotizacion);

            list_manual_repuestos.on('change',function(){
                $('#btnRemoveManualRepuestos').prop('disabled',false);
            });

            list_manual_manoobra.on('change',function(){
               // console.log('selecionado');
                $('#btnRemoveManualManoObra').prop('disabled',false);
            });

            /**
             * ACCIONES PARA IGV
             */
            $('input[type="checkbox"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });

            $('input[type="checkbox"].minimal').on('ifChecked', function(event){
                var tt = getSubtotal();

                if(tt)
                {
                    $('#total_cotizacion').val(getSubtotal()+get_total_igv());
                }
                else
                {
                    alert("El total aún se encuentra en ceros");
                    setTimeout(function(){$('input[type="checkbox"].minimal').iCheck('uncheck');},1000);
                }
            });

            $('input[type="checkbox"].minimal').on('ifUnchecked', function () {
                $('#total_cotizacion').val(get_total_cotizacion());
            });

            /**
             * ACCION PARA GUARDADO DE DATOS EN FORMULARIO
             */
            documento.on('click','#btnSaveCotizacion',function(){

                var en_list_mano_obra          = [],
                    en_list_repuestos          = [],
                    en_list_mano_obra_manual   = [],
                    en_list_repuesto_manual    = [];

                /**
                 * VALIDACIONES DEL FORMULARIO
                 */
                if (input_cliente.val() == '') {
                    alert('El cliente es requerido');
                    return false;
                }

                if (input_vehiculo.val() == '') {
                    alert('El vehículo es requerido');
                    return false;
                }

                if (isNaN(parseInt(input_encargado.val()))) {
                    alert('El encargado es requerido');
                    return false;
                }

                if($('#total_cotizacion').val()<0) {
                    alert('El total no puede ser menor a cero');
                    return false;
                }

                /**
                 * RECOGIENDO LOS DATOS DE REPUESTO
                 */
                $('#table_repuestos tbody tr').each(function(){
                    var dat =  $(this).data();
                    en_list_repuestos.push(dat);
                });

                /**
                 * RECOGIENDO LOS DATOS DE MANO DE OBRA
                 */
                $('#table_mano_obra tbody tr').each(function(){
                    var dat =  $(this).data();
                    en_list_mano_obra.push(dat);
                });

                /*RECOGIENDO LOS DATOS MANUALES DE REPUESTOS*/
                $('#list_manual_repuestos option').each(function(){
                    var dat =  $(this).data();
                   // console.log(dat);
                    en_list_repuesto_manual.push(dat);
                });

                /*RECOGIENDO LOS DATOS MANUALES DE MANO DE OBRA*/
                $('#list_manual_mano_obra option').each(function(){
                    var dat =  $(this).data();
                    en_list_mano_obra_manual.push(dat);
                });


                data_envio = {
                    'Idcotiza':{!! isset($data_cotizacion['cotizacion']->Idcotiza)?$data_cotizacion['cotizacion']->Idcotiza:"''" !!},
                    'idcliente': input_cliente.val(),
                    'idvehiculo':input_vehiculo.val(),
					'id_use_vehicle':input_tipo_vehiculo.val(),
                    'encargado':input_encargado.val(),
                    'dscto': get_descuento(),
                    'repuestos':en_list_repuestos,
                    'mano_obra':en_list_mano_obra,
                    'manual_repuestos':en_list_repuesto_manual,
                    'manual_mano_obra':en_list_mano_obra_manual,
                    'total_repuestos':get_total_repuestos(),
                    'total_cotizacion':$('#total_cotizacion').val(),
                    'total_mano_obra':$('#total_mano_obra').val(),
                    'total_subtotal_cotizacion':$('#total_subtotal_cotizacion').val(),
                    'total_manuales_mano_obra':$('#total_manuales_mano_obra').val(),
                    'total_manuales_repuestos':$('#total_manuales_repuestos').val(),
                    'igv':$('#igv_t').val(),
                    'observa':$('#observa').val(),
                    '_token':$("meta[name='csrf-token']").attr('content')
                };

                $.ajax({
                    url: '/asesor/cotizacion/{!! count($data_cotizacion)>0?'editar':'guardar' !!}',
                    type: 'post',
                    data: data_envio,
                    success: function(data, textStatus, jqXHR){
                        if(jqXHR.status == '200')
                        {
                            alert('Los datos Fueron Guardados Correctamente');
                            window.location = "{!! url('/asesor/cotizacion') !!}";
                            btnSaveCotizacion.prop('disabled',false);
                        }
                        else
                        {
                            alert('Tenemos problemas, inténtalo más tarde');
                            btnSaveCotizacion.prop('disabled',false);
                        }
                    },
                    error:function (data) {
                        //console.log(data.responseJSON.message);
                        if(data.responseJSON.message)
                        {
                            for(var jj in data.responseJSON.message)
                            {

                                if($.isArray(data.responseJSON.message[jj]))
                                {
                                    for(var aa in data.responseJSON.message[jj])
                                    {
                                        alert(data.responseJSON.message[jj][aa]);
                                    }
                                }
                                else
                                {
                                    alert(data.responseJSON.message[jj]);
                                }
                            }
                        }
                        else
                        {
                            alert('Tenemos problemas, inténtalo más tarde');
                            btnSaveCotizacion.prop('disabled',false);
                        }
                    },
                    beforeSend:function () {
                        btnSaveCotizacion.prop('disabled',true);
                    }
                });

            });

            /**
             * LLENADO DE CATALOGOS
             */
            input_cliente.change(function(){
                $("input[name='contacto'],input[name='telef1'],input[name='telef2'],input[name='email_address']").val('');

                if(!isNaN(parseInt($(this).val())))
                {
                    $.ajax({
                        url: '/asesor/cotizacion/clientes',
                        type: 'post',
                        data: {'id':$(this).val(),'_token':$("meta[name='csrf-token']").attr('content')},
                        success: function(data, textStatus, jqXHR){
                           
                            if(jqXHR.status == '200')
                            {
                                $("input[name='contacto']").val(data.address);
                                $("input[name='telef1']").val(data.telef1);
                                $("input[name='telef2']").val(data.telef2);
                                $("input[name='email_address']").val(data.email_address);
                            }
                            else
                            {
                                alert('Tenemos problemas, inténtalo más tarde');
                            }
                        },
                        error:function () {
                            alert('Tenemos problemas, inténtalo más tarde');
                        },
                        beforeSend:function () {

                        }
                    });
                }
                else
                {
                    alert('Selecciona un cliente');
                }
            });
            input_vehiculo.change(function(){
                $("input[name='num_motor'],input[name='color'],input[name='anio'],input[name='idmodelo'],input[name='idmarca']").val('');

                if(!isNaN(parseInt($(this).val())))
                {
                    $.ajax({
                        url: '/asesor/cotizacion/vehiculo',
                        type: 'post',
                        data: {'id':$(this).val(),'_token':$("meta[name='csrf-token']").attr('content')},
                        success: function(data, textStatus, jqXHR){

                            if(jqXHR.status == '200')
                            {
                               // console.log(data);
                                $("input[name='num_motor']").val(data.num_motor);
                                $("input[name='color']").val(data.color);
                                $("input[name='anio']").val(data.anio);
                                $("input[name='idmodelo']").val(data.modelo);
                                $("input[name='idmarca']").val(data.marca);

                                get_cat_repuestos(data.idmarca,data.idmodelo,data.anio);
                                get_cat_mano_obra(data.idmarca,data.idmodelo,data.anio);
                            }
                            else
                            {
                                if(data.length<=0)
                                {
                                    alert('No existen registros para esa placa');
                                    return false;
                                }

                                alert('Tenemos problemas, inténtalo más tarde');
                            }

                        },
                        error:function () {
                            alert('Tenemos problemas, inténtalo más tarde');
                        },
                        beforeSend:function () {

                        }
                    });
                }
                else
                {
                    alert('Selecciona un vehículo');
                }
            });

            /**
             * ACCIONES PARA AGREGAR REPUESTOS
             */
            btnAddRepuesto.click(function () {
                item = cat_repuestos.find(':selected').data();
                //console.log(item);
                if(Object.keys(item).length>0)
                {
                    var edit = '';
                    if(item.modificar)
                    {
                        edit = '&nbsp;<i class="fa fa-pencil icon_edit" style="color:blue;margin-top: 11px;cursor: pointer;"></i>';
                    }
                    contenido = ' <tr data-p1="' + item.precio +'" data-d1="' + item.descripcion + '" data-codigo="'+item.codigo+'" data-iditem="'+item.iditem+'" data-descripcion="'+item.descripcion+'" data-precio="'+item.precio+'"><td>'+item.descripcion+'</td> <td>'+item.precio+'</td><td>'+item.list_price_per_unit+'</td><td>'+item.cant_sugerida+'</td><td><input type="text" class="form-control repo_cantidad" ></td> <td></td> <td style="text-align: center;"><i class="fa fa-remove icon_remove" style="color:red;margin-top: 11px;cursor: pointer;"></i>'+edit+'</td> </tr>';
                    //contenido = ' <tr data-codigo="'+item.codigo+'" data-iditem="'+item.iditem+'" data-nombre="'+item.descripcion+'" data-precio="'+item.precio+'"> <td>'+item.codigo+'</td> <td>'+item.descripcion+'</td> <td>'+item.precio+'</td><td>'+item.list_price_per_unit+'</td><td>'+item.cant_sugerida+'</td><td><input type="text" class="form-control repo_cantidad" ></td> <td></td> <td style="text-align: center;"><i class="fa fa-remove icon_remove" style="color:red;margin-top: 11px;cursor: pointer;"></i>'+edit+'</td> </tr>';
                    tbl_repuestos.find('tbody').append(contenido);
                    //cat_repuestos.val('');
                    //console.log(item);
                }
                else
                {
                    alert('No tienes seleccionado ningun repuestos');
                }

            });
            documento.on('change','.repo_cantidad',function () {
                row = $(this).parents('tr');

                item = row.data();

               // console.log(item);
                if(!isNaN(item.precio))
                {
                    index = $(this).closest('tr').index();

                    var tt =  $(this).val()*item.precio;

                    $('#table_repuestos tbody tr:eq('+index+')').data('cantidad',$(this).val());
                    $('#table_repuestos tbody tr:eq('+index+')').data('subtotal',tt);
                    $('#table_repuestos tbody tr:eq('+index+') td:eq(5)').text(tt);

                    get_total_cotizacion();
                }
                else
                {
                    alert('No tenemos el precio de este artículo');
                }

            });
            documento.on('click','#save_repuestos',function () {


                $('.bs-repuestos-modal-lg').modal('hide');
                get_total_cotizacion();
            });

            /**
             * ACCIONES PARA AGREGAR MANO DE OBRA
             */
            btnAddManoObra.click(function(){
                item = cat_mano_obra.find(':selected').data();

                    var preciomod = $('#mano_obra_precio_moodif').val();
                    
                    //console.log(preciomod < item.precio );
                    if(preciomod < item.precio)
                        { // validar que el precio ingresado no sea menor que el precio que viene por defecto en la base de datos
                            alert ('el precio modificado no puede ser menor que el precio base');
                            return false;
                        }else{
                            if(Object.keys(item).length>0)
                                {

                                        var edit = '';
                                   // if(item.condicion)
                                    //{
                                        edit = '&nbsp;<i class="fa fa-pencil icon_edit_mano" style="color:blue;margin-top: 11px;cursor: pointer;"></i>';
                                    //}
                                    
                                            //contenido = '<tr data-iditem="'+item.iditem+'" data-nombre="'+item.nombre+'" data-precio="'+item.precio+'"  data-precio-modificado="'+$('#mano_obra_precio_moodif').val()+'"> <td>'+item.nombre+'</td> <td>'+ item.precio +'</td> <td><input type="text" class="form-control labor_cantidad" ></td> <td></td> <td style="text-align: center;"><i class="fa fa-remove icon_remove" style="color:red;margin-top: 11px;cursor: pointer;"></i>'+edit+'</td> </tr>';
                                            contenido = '<tr data-iditem="'+item.iditem+'" data-descripcion="'+item.descripcion+'" data-precio="'+item.precio+'" data-precio-modificado="'+$('#mano_obra_precio_moodif').val()+'"> <td>'+item.descripcion+'</td> <td>'+ item.precio +'</td> <td>'+$('#mano_obra_precio_moodif').val()+'</td><td style="text-align: center;"><i class="fa fa-remove icon_remove" style="color:red;margin-top: 11px;cursor: pointer;"></i>'+edit+'</td> </tr>';
                                        //contenido = '<tr data-iditem="'+item.iditem+'" data-nombre="'+item.nombre+'" data-precio="'+item.precio+'" data-precio-modificado="'+$('#mano_obra_precio_moodif').val()+'"> <td>'+item.nombre+'</td> <td>'+ 000 +'</td> <td><input type="text" class="labor_cantidad" value="'+item.precio+'"></td> <td>'+$('#mano_obra_precio_moodif').val()+'</td> <td style="text-align: center;"><i class="fa fa-remove icon_remove" style="color:red;margin-top: 11px;cursor: pointer;"></i></td> </tr>';
                        
                        

                                        tbl_mano_obra.find('tbody').append(contenido);
                                    
                                        $('#mano_obra_precio_ivg,#mano_obra_precio_moodif').val(0);
                                        get_total_cotizacion();
                                    }
                                    else
                                    {
                                        alert('No tienes seleccionado ninguna mano de obra');
                                    }
                    }

                        

            });

            cat_mano_obra.change(function(){
                item = cat_mano_obra.find(':selected').data();
                //console.log(item);
                $('#mano_obra_precio_ivg,#mano_obra_precio_moodif').val(item.precio);
            });

           // documento.on('change','#mano_obra_precio_moodif',val_mano_precio);

           
            
            documento.on('change', '.labor_cantidad',function(){

               
               
                row = $(this).parents('tr');
               
                            item = row.data();
                            //console.log(item);
                            if(!isNaN(item.precio))
                            {

                                index = $(this).closest('tr').index();
                                
                                var tt =  item.precio;
                               // console.log(tt);
                                //console.log(index);
                                $('#table_mano_obra tbody tr:eq('+index+')').data('precio',$(this).val());
                                //$('#table_repuestos tbody tr:eq('+index+')').data('cantidad',$(this).val());
                                $('#table_mano_obra tbody tr:eq('+index+')').data('subtotal',$(this).val());
                                $('#table_mano_obra tbody tr:eq('+index+') td:eq(6)').text(tt);

                                get_total_cotizacion();
                            }
                            else
                            {
                                alert('No tenemos el precio de este artículo');
                            }
            });

            documento.on('click','#btnAddTableMano',function () {
                $('.bs-manoObra-modal-lg').modal('hide');
                /**agregando esta linea para comprobar */
                get_total_cotizacion();
            });

            /**
             * ACCIONES PARA AGREGAR REPUESTOS MANUAL
             */
            documento.on('click','#btnAddManualRepuestos',function(){
                
                var desc = $('#input_desc_manual_repuestos').val(),
                    pre  = $('#input_precio_manual_repuestos').val(),
					orden = $('#input_orden_manual_repuesto').val(),
                    cant = $('#input_cantidad_manual_respuesto').val();

                $('#list_manual_repuestos').append('<option value="" data-manual-orden="'+orden+'" data-manual-cant="'+cant+'" data-manual-desc="'+desc+'" data-manual-precio="'+pre+'">'+ orden + '-' + desc +' &nbsp'+' &nbsp' +' '+ pre +' &nbsp'+ cant +'</option>');

                $('#input_orden_manual_repuesto,#input_desc_manual_repuestos,#input_precio_manual_repuestos,#input_cantidad_manual_respuesto').val('');

                $('#btnAddManualRepuestos').prop('disabled',true);

                get_total_cotizacion();
            });

            documento.on('keyup','#input_orden_manual_repuesto,#input_desc_manual_repuestos,#input_precio_manual_repuestos,#input_cantidad_manual_respuesto',function () {
                if($('#input_precio_manual_repuestos').val()!='' && $('#input_desc_manual_repuestos').val()!='' && $('#input_cantidad_manual_respuesto').val()!='')
                {
                    $('#btnAddManualRepuestos').prop('disabled',false);
                }
                else
                {
                    $('#btnAddManualRepuestos').prop('disabled',true);
                }
            });
            documento.on('click','#btnRemoveManualRepuestos',function(){
                $('#list_manual_repuestos').find(':selected').each(function() {
                    $(this).remove();
                });
                $('#btnRemoveManualRepuestos').prop('disabled',true);
                get_total_cotizacion();
            });

            /**
             * ACCIONES PARA AGREGAR MANO DE OBRA MANUAL
             */
            documento.on('click','#btnAddManualManoObra',function(){
                var desc = $('#input_desc_manual_mano_obra').val(),
					orden = $('#input_orden_manual_mano_obra').val(),
                    pre  = $('#input_precio_manual_mano_obra').val();
                    //cant = $('#input_cantidad_manual_mano_obra').val();

                $('#list_manual_mano_obra').append('<option value=""  data-manual-orden="'+orden+'" data-manual-desc="'+desc+'" data-manual-precio="'+pre+'">'+orden + '-' + desc + ' ' + pre +' </option>');

                $('#input_orden_manual_mano_obra,#input_desc_manual_mano_obra,#input_precio_manual_mano_obra').val('');

                $('#btnAddManualManoObra').prop('disabled',true);

                get_total_cotizacion();
            });
            documento.on('keyup','#input_desc_manual_mano_obra,#input_precio_manual_mano_obra',function () {
                if($('#input_desc_manual_mano_obra').val()!='' && $('#input_precio_manual_mano_obra').val()!='')
                {
                    $('#btnAddManualManoObra').prop('disabled',false);
                }
                else
                {
                    $('#btnAddManualManoObra').prop('disabled',true);
                }
            });
            documento.on('click','#btnRemoveManualManoObra',function(){
                $('#list_manual_mano_obra').find(':selected').each(function() {
                    $(this).remove();
                });
                $('#btnRemoveManualManoObra').prop('disabled',true);
                get_total_cotizacion();
            });



/**------------------------------------------------------------------------------------------------------------------------------------------------------------------- */


/**
             * Acciones para editar precio MANO DE OBRA
             * */
            documento.on('click','.icon_edit_mano',function () {
                //console.log('presionandolo');
                var row = $(this).parents('tr').data();

               // console.log(row);
                
                $(this).parents('tr').find('td:nth-child(1)').html('<div class="form-group"><input type="text" value="'+row.descripcion+'" class="form-control new_des"></div>');
                
                $(this).parents('tr').find('td:nth-child(3)').html('<div class="form-group"><input type="text" value="'+row.precio+'" class="form-control new_prec"></div>');
            });

            documento.on('change','.new_des',function () {
                var row = $(this).parents('tr').data();
                var desc = $(this).val();
                //console.log(row);
                row.descripcion = desc;

            });

            documento.on('change','.new_prec',function () {

               
                var row = $(this).parents('tr').data();
                var pres = parseFloat($(this).val());

                //console.log('precio nuevo: ' + pres);
                //console.log('precio Base: ' + row.precio);
                //console.log(row);
                
               /* if(pres < row.p1){
                        alert('El precio modificado no debe ser menor que el precio base');
                        $(this).val(row.precio);
                        $('#btnAddTableMano').attr('disabled', true);
                        $('#save_repuestos').attr('disabled', true);
                        return false;
                }else{*/
                    
                    row.precioModificado = pres;
                
                        row.subtotal = parseFloat(row.precioModificado);
                    // console.log($('#table_mano_obra tbody tr:eq('+index+') td:eq(1)'));
                        var index = $(this).closest('tr').index();
                        //$('#table_mano_obra tbody tr:eq('+index+') td:eq(3)').text($(this).val());
                        //$('#table_mano_obra tbody tr:eq('+index+') td:eq(5)').text($(this).val());
                        $('#btnAddTableMano').attr('disabled', false);
                        $('#save_repuestos').attr('disabled', false);
                        get_total_cotizacion();


                //}
                   
            });










/**------------------------------------------------------------------------------------------------------------------------------------------------------------------ */

            /**
             * Acciones para editar precio
             * */
            documento.on('click','.icon_edit',function () {
                var row = $(this).parents('tr').data();
                //console.log(row);
                $(this).parents('tr').find('td:nth-child(1)').html('<div class="form-group"><input type="text" value="'+row.descripcion+'" class="form-control new_des"></div>');
                $(this).parents('tr').find('td:nth-child(2)').html('<div class="form-group"><input type="number" value="'+row.precio+'" class="form-control new_prec"></div>');
            });

            documento.on('change','.new_des',function () {
                var row = $(this).parents('tr').data();
                var desc = $(this).val();
                row.descripcion = desc;
            });

            documento.on('change','.new_prec',function () {
                var row = $(this).parents('tr').data();
                var pres = $(this).val();
                row.precio = pres;
                row.subtotal = parseFloat(row.cantidad) * parseFloat(row.precio);
                var index = $(this).closest('tr').index();
                $('#table_repuestos tbody tr:eq('+index+') td:eq(5)').text($(this).val()*row.cantidad);
                get_total_cotizacion();
            });

            /**
             * FUNCIONES
             */
            function get_cat_repuestos(marca,modelo,anio) {
                $.ajax({
                    url: '/asesor/cotizacion/repuestos',
                    type: 'post',
                    data: {
                        'anio':anio,
                        'idmodelo':modelo,
                        'idmarca':marca,
                        '_token':$("meta[name='csrf-token']").attr('content')
                    },
                    success: function(data2, textStatus2, jqXHR2){

                        if(jqXHR2.status == '200')
                        {
                            if(data2.length>0)
                            {
                                contenido = '';
                            //console.log(data2);
                                cat_repuestos.html(contenido);

                                $('#table_repuestos tbody').html(contenido);

                                contenido = '<option value="">Selecciona ...</option>';

                                get_total_cotizacion();

                                $.each(data2, function( index, value ) {
                                    contenido += '<option value="'+value.iditem+'" data-iditem="'+value.iditem+'" data-codigo="'+value.codigo+'" data-precio="'+value.list_price_per_unit+'" data-descripcion="'+value.descripcion+'" data-modificar="'+value.allow_change_price+'" data-cant_sugerida="'+(value.cant_sugerida!=null?value.cant_sugerida:0)+'" data-list_price_per_unit="'+(value.list_price_per_unit!=null?value.list_price_per_unit:0)+'"> ' + value.descripcion + '</option>';
                                });

                                cat_repuestos.html(contenido);
                                //console.log(contenido);
                                $('#btn-repuestos-modal-lg').prop('disabled',false)
                            }
                            else
                            {
                                alert('No tenemos repuestos para este vehiculo');
                                $("#cat_repuestos").html('');
                                $('#table_repuestos tbody').html('');
                                $('#btn-repuestos-modal-lg').prop('disabled',false);
                            }
                        }
                        else
                        {
                            alert('Tenemos problemas, inténtalo más tarde');
                        }
                    },
                    error:function () {
                        alert('Tenemos problemas, inténtalo más tarde');
                    },
                    beforeSend:function () {

                    }
                });
            }

            function get_cat_mano_obra(marca,modelo,anio) {
                $.ajax({
                    url: '/asesor/cotizacion/mano-obra',
                    type: 'post',
                    data: {
                        'anio':anio,
                        'idmodelo':modelo,
                        'idmarca':marca,
                        '_token':$("meta[name='csrf-token']").attr('content')
                    },
                    success: function(data3, textStatus3, jqXHR2){

                        if(jqXHR2.status == '200')
                        {
                            if(data3.length>0)
                            {
                                contenido = '';

                                cat_mano_obra.html(contenido);

                                $('#table_mano_obra tbody').html(contenido);

                                contenido = '<option value="">Selecciona ...</option>';

                                get_total_cotizacion();

                                //console.log(data3);

                                $.each(data3, function( index, value ) {
                                    contenido +='<option value="'+value.idlabor+'" data-condicion="'+value.condicion+'" data-descripcion="'+value.nombrelabor+'" data-precio="'+value.subtotal+'" data-iditem="'+value.idlabor+'">'+value.nombrelabor+'</option>';
                                });

                                //console.log(contenido);

                                cat_mano_obra.html(contenido);
                                $('#btn-manoObra-modal-lg').prop('disabled',false);
                            }
                            else
                            {
                                alert('No tenemos mano de obra para este vehiculo');
                                $("#cat_mano_obra").html('');
                                $('#btn-manoObra-modal-lg').prop('disabled',true);
                            }
                        }
                        else
                        {
                            alert('Tenemos problemas, inténtalo más tarde');
                        }
                    },
                    error:function () {
                        alert('Tenemos problemas, inténtalo más tarde');
                    },
                    beforeSend:function () {

                    }
                });
            }

            function get_total_cotizacion() {

                var total = 0,
                    sub   = getSubtotal(),
                    
                    igv = get_total_igv();


                total = parseFloat( sub+igv );
                
                    //console.log(sub);
                    //console.log(igv);
                    //console.log(total);
                    
                //$('#igv').val(igv.toFixed(2));
                $('#total_cotizacion').val(total.toFixed(2));
                return total;
            }

            function getSubtotal(){
                var subtotal1 =  get_total_repuestos() + get_total_mano_obra() + get_total_manual_repuestos() + get_total_manual_mano_obra(),
                    desc  = get_descuento();
               
                if(desc > 0)
                {
                    desc1 = (subtotal1 * desc)/100;
                    subtotal1 = subtotal1 - desc1;
                    //$('#importe_dsc').val(desc1.toFixed(2));
                     $('#importe_dsc').val(subtotal1.toFixed(2));
                }else{

                    desc1 = 0;
                    subtotal1 = subtotal1 - desc1;
                    $('#importe_dsc').val(desc1.toFixed(2));
                    // $('#importe_dsc').val(subtotal1.toFixed(2));
                }

                    
                var verify = $('#verify').val();


                if(verify == 'Y')
                {
                    var total = subtotal1 / 1.18;
                }
                else
                {
                    var total = subtotal1 / 1;
                   
                }




                $('#total_subtotal_cotizacion').val(total.toFixed(2));


                return total;
            }

            function get_total_repuestos() {
                total = 0;

                $('#table_repuestos tbody tr').each(function() {
                    var ll = $(this).data();
                    //console.log(ll);
                    total += parseFloat(ll.subtotal);
                });

                /**
                 * TOTAL DEL MODAL MANO DE OBRA
                 */
                $('#table_repuestos tfoot tr:eq(0) td:eq(1)').html('$'+total);

                /**
                 * TOTAL DEL INPUT DE REPUESTOS
                 */
                $('#total_repuestos').val(total);

               // console.log('repuestos ' + total);
                return total;
            }

            function get_total_mano_obra() { /**---------------------------------------------------------------------------------- */
                var total = 0;

                $('#table_mano_obra tbody tr').each(function(){
                    var dat =  $(this).data();
                    total += parseFloat(dat.precioModificado);
                    //console.log(dat);
                });

                $('#total_mano_obra').val(total);

                /**
                 * TOTAL DEL MODAL MANO DE OBRA     ************************************************************************************
                 */
                $('#table_mano_obra tfoot tr:eq(0) td:eq(1)').html('$'+total);

                
                return total;
            }

            function get_total_manual_repuestos() {
                var total = 0;
                $('#list_manual_repuestos option').each(function(){
                    var dat =  $(this).data();
                    //console.log(dat);
                    if(dat.manualCant == undefined){
                        total += parseFloat(dat.manualPrecio);
                    }else{
                        total += parseFloat(dat.manualPrecio*dat.manualCant);
                    }
                   // console.log(dat.manualCant);
                    
                });
                $("#total_manuales_repuestos").val(total);

              //  console.log('Manual repuestos '+ total);

                return total;
            }

            function get_total_manual_mano_obra() {
                var total = 0;
                $('#list_manual_mano_obra option').each(function(){
                    var dat =  $(this).data();
                    total += parseFloat(dat.manualPrecio);
                });

               // console.log('Manual mano obra '+ total);

                $("#total_manuales_mano_obra").val(total);
                return total;
            }

            function get_total_igv() {

               var check = $('input[type="checkbox"].minimal').is(':checked');
               var suma = get_total_repuestos() + get_total_mano_obra() + get_total_manual_repuestos() + get_total_manual_mano_obra();


               var sub = getSubtotal();
              
                var igv = suma - sub;

                $('#igv_t').val(igv.toFixed(2));

                    return igv;
               
               

            }

            function val_mano_precio()
            {

            }

            function get_descuento() {
                var des = 0;
                var numero = $('#total_descuento_cotizacion').val();



                if (isNaN(numero)){
                    alert ("Ups... " + numero + " no es un número.");
                    $('#total_descuento_cotizacion').val(0);
                    return des;
                }
                else{
                   // numero = parseInt(numero);
                        numero = parseFloat(numero);

                    //if (numero % 1 == 0) {

                        if(numero > 10)
                        {
                            alert ("El numero debe ser menor a 10");
                            $('#total_descuento_cotizacion').val(0);
                            return des;
                        }
                        else
                        {
                            des = (numero);
                           // console.log('descuento '+ des);
                            //console.log(des);
                            return des;
                        }

                   // }
                   // else{
                      //  alert ("El numero no es entero");
                      //  $('#total_descuento_cotizacion').val(0);
                      //  return des;
                   // }
                }


            }

            /**
             * ACCIONES GENERALES
             */
            documento.on('click', ".icon_remove", function (e) {
                $(this).closest('tr').remove();
                get_total_cotizacion();
            });

        });
    </script>
@endsection