<div class="col-md-5">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Datos Generales</h3>
        </div>	
 
		<div class="form-group row">
			<label class="col-sm-4 col-form-label-sm"align=right >* Cliente</label>
			<div class="col-sm-6">
				<div>
					<select name="customer_id" id="customer_id" class="form-control select2" required="true">
						<option value=""></option>
						@foreach($clientes as $sucur)
							<option value="{{ $sucur->idcliente }}">{{ $sucur->full_name }}</option>
						@endforeach
					</select>	
				</div>
			</div>		
			<label  >Nuevo</label>			
		</div>
		<div class="form-group row">
			<label class="col-sm-4 col-form-label-sm"align=right >* Placa Vehiculo</label>
			<div class="col-sm-6">
				<div>
					<select name="placa_id" id="placa_id" class="form-control select2" required="true">
						<option value=""></option>
						@foreach($vehiculos as $sucur)
							<option value="{{ $sucur->id }}">{{ $sucur->placa }}</option>
						@endforeach
					</select>	
				</div>
			</div>																
		</div>
		
		<div class="form-group row">
			<label class="col-sm-4 col-form-label-sm"align=right >* Sucursal</label>
			<div class="col-sm-6">
				<div>
					<select name="site_id" id="site_id" class="form-control select2" required="true">
						<option value=""></option>
						@foreach($sucursales as $sucur)
							<option value="{{ $sucur->id }}">{{ $sucur->name }}</option>
						@endforeach
					</select>	
				</div>
			</div>																
		</div>
		
		 
		<div class="form-group">
			<label>Fecha de Cita: Fecha y Hora</label>
			<input type="datetime-local" name="datetime_reservation"   id="datetime_reservation" 
			required="true" class="form-control form-control-sm"  >			 
		</div>		
		
	
		
 
    </div>
</div>
 
<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Informacion de Servicio</h3>
        </div>		  
		
		<div class="form-group row">
			<label class="col-sm-4 col-form-label-sm"align=right >* Servicio 1</label>
			<div class="col-sm-6">
				<div>
					<select name="service1_id" id="service1_id" class="form-control select2" required="true">
						<option value=""></option>
						@foreach($labores as $sucur)
							<option value="{{ $sucur->idlabor }}">{{ $sucur->nombrelabor }}</option>
						@endforeach
					</select>	
				</div>
			</div>																
		</div>
		
		<div class="form-group row">
			<label class="col-sm-4 col-form-label-sm"align=right >* Servicio 2</label>
			<div class="col-sm-6">
				<div>
					<select name="service2_id" id="service2_id" class="form-control select2" required="true">
						<option value=""></option>
						@foreach($labores as $sucur)
							<option value="{{ $sucur->idlabor }}">{{ $sucur->nombrelabor }}</option>
						@endforeach
					</select>	
				</div>
			</div>																
		</div>
		 
		 
    </div>
</div> 