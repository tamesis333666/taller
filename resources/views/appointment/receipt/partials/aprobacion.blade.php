<div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Informacion de Administrador</h3>
        </div> 
		<div class="form-group">
            {!! Form::label('coffin_code', 'Codigo Ataud') !!}
            {!! Form::text('coffin_code', (isset($service->coffin_code)) ? $service->coffin_code : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
        </div>
		<div class="form-group">
            {!! Form::label('batch_coffin', 'Lote Ataud') !!}
            {!! Form::text('batch_coffin', (isset($service->batch_coffin)) ? $service->batch_coffin : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
        </div>		 
		<div class="form-group" >
            <div class="form-group row">
				<label class="col-sm-3 col-form-label-sm" align="left">Capilla </label>
				<div class="col-sm-9">
					<div>
						{!! Form::select('chapel_id', $capilla, (isset($service->chapel_id)) ? $service->chapel_id : null, ['class' => 'form-control select2', ''])!!}
					</div>
				</div>											
			</div>
        </div>		
		<div class="form-group" >
            <div class="form-group row">
				<label class="col-sm-3 col-form-label-sm"align=left >Fecha </label>
				<div class="col-sm-9">
					<div>
						<input  type="date" name="chapel_date" class="form-control form-control-sm"
						value="{{(isset($service->chapel_date)) ? $service->chapel_date : null}}">	
					</div>
				</div>											
			</div>
        </div>		
		<div class="form-group" >
            <div class="form-group row">
				<label class="col-sm-3 col-form-label-sm"align=left >Hora </label>
				<div class="col-sm-9">
					<div>
						<input type="time" name="chapel_time" class="form-control form-control-sm"
						value="{{(isset($service->chapel_time)) ? $service->chapel_time : null}}">	
					</div>
				</div>											
			</div>
        </div>		 
    </div>
</div>

<div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Informacion Movilidad </h3>
        </div>		
			<div class="form-group" >
		<div class="form-group row">
			<label class="col-sm-3 col-form-label-sm" align="left">Conductor #1</label>
			<div class="col-sm-9">
				<div>
					{!! Form::text('driver1', (isset($service->driver1)) ? $service->driver1 : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
				</div>
			</div>											
		</div>
	</div>	

    </div>
	
	<div class="form-group" >
		<div class="form-group row">
			<label class="col-sm-3 col-form-label-sm" align="left">Conductor #2</label>
			<div class="col-sm-9">
				<div>
					{!! Form::text('driver2', (isset($service->driver2)) ? $service->driver2 : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
				</div>
			</div>											
		</div>
	</div>		
	
	<div class="form-group" >
		<div class="form-group row">
			<label class="col-sm-3 col-form-label-sm" align="left">Porta Flores</label>
			<div class="col-sm-9">
				<div>
					{!! Form::text('flower_holder', (isset($service->flower_holder)) ? $service->flower_holder : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
				</div>
			</div>											
		</div>
	</div>	

	<div class="form-group" >
		<div class="form-group row">
			<label class="col-sm-3 col-form-label-sm" align="left">Carroza</label>
			<div class="col-sm-9">
				<div>
					{!! Form::text('carroza', (isset($service->carroza)) ? $service->carroza : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
				</div>
			</div>											
		</div>
	</div>

	<div class="form-group" >
		<div class="form-group row">
			<label class="col-sm-3 col-form-label-sm" align="left">Limosina</label>
			<div class="col-sm-9">
				<div>
					{!! Form::text('limousine', (isset($service->limousine)) ? $service->limousine : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
				</div>
			</div>											
		</div>
	</div>

	<div class="form-group" >
		<div class="form-group row">
			<label class="col-sm-3 col-form-label-sm" align="left">Placa Limosina</label>
			<div class="col-sm-9">
				<div>
					{!! Form::text('limousine_plate', (isset($service->limousine_plate)) ? $service->limousine_plate : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
				</div>
			</div>											
		</div>
	</div>	
</div> 



<div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Datos Adicionales </h3>
        </div>		
 

		<div class="form-group">
            <div class="form-group row">
				<label class="col-sm-4 col-form-label-sm"align=left >Ataud-Urna</label>
				<div class="col-sm-6">
					<div>
						{!! Form::text('casket_urn', (isset($service->casket_urn)) ? $service->casket_urn : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}

					</div>
				</div>											
			</div>
        </div>	
		
    </div>
	
	
	<div class="form-group">
		<div class="form-group row">
			<label class="col-sm-4 col-form-label-sm"align=left >Formol</label>
			<div class="col-sm-6">
				<input class="form-check-input" value="S" type="radio" name="formaldehyde" 
				{{ (isset($service->formaldehyde) && $service->formaldehyde == 'S') ? "checked": ""}}>
				<label class="form-check-label" >
				Si
				</label>&nbsp &nbsp &nbsp  &nbsp  &nbsp 
				<input class="form-check-input" value="N" type="radio" name="formaldehyde" 
				{{ (isset($service->formaldehyde) && $service->formaldehyde == 'N') ? "checked": ""}}>
				<label class="form-check-label" >
				No
				</label>
			 </div>	
		</div>	
	</div>	
	
	<div class="form-group" >
		<div class="form-group row">
			<label class="col-sm-3 col-form-label-sm" align="left">Cargadores</label>
			<div class="col-sm-9">
				<div>
					{!! Form::text('chargers', (isset($service->chargers)) ? $service->chargers : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
				</div>
			</div>											
		</div>
	</div>	

	<div class="form-group" >
		<div class="form-group row">
			<label class="col-sm-3 col-form-label-sm" align="left">Otros</label>
			<div class="col-sm-9">
				<div>
					{!! Form::text('others', (isset($service->others)) ? $service->others : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
				</div>
			</div>											
		</div>
	</div>	

	<div class="form-group" >
		<div class="form-group row">
			<label class="col-sm-3 col-form-label-sm" align="left">Agencia</label>
			<div class="col-sm-9">
				<div>
					{!! Form::text('agency', (isset($service->agency)) ? $service->agency : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
				</div>
			</div>											
		</div>
	</div>	
 
</div> 