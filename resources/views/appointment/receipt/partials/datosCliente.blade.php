<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Datos Fallecimiento</h3>
        </div>
		 
		<div class="form-group">
            {!! Form::label('customer_id', 'Fallecido') !!}
            {!! Form::select('customer_id', $clientes, null, ['class' => 'form-control select2', ''])!!}
        </div>
	 
		<div class="form-group">
			<label    >Fallecimiento: Fecha y Hora</label>
			<input  type="datetime-local" name="fectimedeath"  class="form-control form-control-sm">
			 
		</div>
		
		<div class="form-group">
            <div class="form-group row">
				<label class="col-sm-4 col-form-label-sm"align=right >Fact</label>
				<div class="col-sm-6">
					<div>
						<input  type="text" name="fact"  class="form-control form-control-sm">	
					</div>
				</div>											
			</div>
        </div>
		
		<div class="form-group">
            <div class="form-group row">
				<label class="col-sm-4 col-form-label-sm"align=right >Certificado</label>
				<div class="col-sm-6">
					<div>
						<input  type="text" name="certificate"  class="form-control form-control-sm">	
					</div>
				</div>											
			</div>
        </div>
		 
    </div>
</div>

<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Informacion Salida</h3>
        </div>
		 
		<div class="form-group">
            {!! Form::label('requestor_id', 'Nombre Solicitante') !!}
            {!! Form::select('requestor_id', $clientes, null, ['class' => 'form-control select2', ''])!!}
        </div>
		<div class="form-group">
            {!! Form::label('exit_address', 'Direccion Salida') !!}
            {!! Form::select('exit_address', $clientes, null, ['class' => 'form-control select2', ''])!!}
            
        </div>
		<div class="form-group">
           {!! Form::label('exit_hour', 'Hora Salida') !!}
			{!! Form::time('exit_hour', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
			 
        </div>
		
		<div class="form-group">
			   {!! Form::label('installation_address', 'Direccion Instalacion') !!}
				{!! Form::textarea('installation_address', null, ['class' => 'form-control' , 'placeholder' => 'Ingrese Direccion Instalacion' , 'rows' => 2, 'cols' => 40 , 'placeholder' => '', 'required']) !!}	 
        </div>
		 
    </div>
</div>
 
 
<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Informacion de despacho</h3>
        </div>
		 
		<div class="form-group">
            {!! Form::label('dispatch_date', 'Fecha Despacho') !!}
            {!! Form::date('dispatch_date',  null, ['class' => 'form-control select2', ''])!!}
        </div>
		<div class="form-group">
            {!! Form::label('dispatch_type', 'Tipo Despacho') !!}
            {!! Form::select('dispatch_type', $clientes, null, ['class' => 'form-control select2', ''])!!}
            
        </div>
		<div class="form-group">
            {!! Form::label('dispatch_type', 'Tipo Movilidad') !!}
            {!! Form::select('dispatch_type', $clientes, null, ['class' => 'form-control select2', ''])!!}
        </div>
		 
    </div>
</div>

<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Informacion Complementaria</h3>
        </div>
		
		<label class="form-check-label" > Custodia </label>
		<div class="form-group">
				<div class="form-check">
				  <input class="form-check-input" value="S" type="radio" name="opt_custodia" checked>
				  <label class="form-check-label" >
					Si
				  </label>&nbsp &nbsp &nbsp  &nbsp  &nbsp
				  <input class="form-check-input" value="S" type="radio" name="opt_custodia" checked>
				  <label class="form-check-label" >
					No
				  </label>
				</div>
			 
		</div>
		<label class="form-check-label" > Embalsanamiento </label>
		<div class="form-group">
				<div class="form-check">
				  <input class="form-check-input" value="S" type="radio" name="opt_embalsanmiento" checked>
				  <label class="form-check-label" >
					Si
				  </label>&nbsp &nbsp &nbsp  &nbsp  &nbsp
				  <input class="form-check-input" value="S" type="radio" name="opt_embalsanmiento" checked>
				  <label class="form-check-label" >
					No
				  </label>
				</div>
			 
		</div>		
		
		<div class="form-group">
            {!! Form::label('dispatch_type', 'Cementerio') !!}
            {!! Form::select('dispatch_type', $clientes, null, ['class' => 'form-control select2', ''])!!}
            
        </div>

		{{-- <div class="form-group">
			   {!! Form::label('installation_address', 'Observacion') !!}
				{!! Form::textarea('installation_address', null, ['class' => 'form-control' , 'placeholder' => 'Ingrese Direccion Instalacion' , 'rows' => 2, 'cols' => 40 , 'placeholder' => '', 'required']) !!}	 
        </div>		 --}}
		
		<div class="form-group">
            {!! Form::label('dispatch_type', 'Tipo Movilidad') !!}
            {!! Form::select('dispatch_type', $clientes, null, ['class' => 'form-control select2', ''])!!}
        </div>
		 
    </div>
</div>