<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Informacion de despacho</h3>
        </div>		 
		<div class="form-group">
            {!! Form::label('dispatch_date', 'Fecha Despacho') !!}
            {!! Form::date('dispatch_date',  (isset($service->dispatch_date)) ? $service->dispatch_date : null, ['class' => 'form-control select2', ''])!!}
        </div>
		<div class="form-group" >
            <div class="form-group row">
				<label class="col-sm-3 col-form-label-sm"align=left >Hora Despacho</label>
				<div class="col-sm-9">
					<div>
						<input  type="time" name="dispatch_time"  class="form-control form-control-sm"
						 value="{{(isset($service->dispatch_time)) ? $service->dispatch_time : null}}">	
					</div>
				</div>											
			</div>
        </div>		 
		<div class="form-group">
            {!! Form::label('dispatch_type', 'Tipo Despacho') !!}
            {!! Form::select('dispatch_type', $tipodes, (isset($service->dispatch_type)) ? $service->dispatch_type : null, ['class' => 'form-control select2', ''])!!}
        </div>
		<div class="form-group">
            {!! Form::label('mobility_type', 'Tipo Movilidad') !!}
            {!! Form::select('mobility_type', $tipomov, (isset($service->mobility_type)) ? $service->mobility_type : null, ['class' => 'form-control select2', ''])!!}
        </div>		 
    </div>
</div> 
<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">  Complementaria 1</h3>
        </div>		
		<label class="form-check-label" > Custodia </label>
		<div class="form-group">
			<div class="form-check">
				<input class="form-check-input" value="S" type="radio" name="custody_flag" 
				{{ (isset($service->custody_flag) && $service->custody_flag == 'S') ? "checked": ""}}>
				<label class="form-check-label" >
				Si
				</label>&nbsp &nbsp &nbsp  &nbsp  &nbsp
				<input class="form-check-input" value="N" type="radio" name="custody_flag" 
				{{ (isset($service->custody_flag) && $service->custody_flag == 'N') ? "checked": ""}}>
				<label class="form-check-label" >
				No
				</label>
			</div>			 
		</div>
		<label class="form-check-label" > Embalsanamiento </label>
		<div class="form-group">
			<div class="form-check">
				<input class="form-check-input" value="S" type="radio" name="embalming_flag" 
				{{ (isset($service->embalming_flag) && $service->embalming_flag == 'S') ? "checked": ""}}>
				<label class="form-check-label" >
				Si
				</label>&nbsp &nbsp &nbsp  &nbsp  &nbsp
				<input class="form-check-input" value="N" type="radio" name="embalming_flag" 
				{{ (isset($service->embalming_flag) && $service->embalming_flag == 'N') ? "checked": ""}}>
				<label class="form-check-label" >
				No
				</label>
			</div>			 
		</div>
		<div class="form-group">
            <div class="form-group row">
				<label class="col-sm-4 col-form-label-sm"align=left >Nicho</label>
				<div class="col-sm-6">
					<div>
						<input type="text" name="niche" class="form-control form-control-sm"
						value="{{ (isset($service->niche)) ? $service->niche: ""}}">	
					</div>
				</div>											
			</div>
        </div>		
		<div class="form-group">
            {!! Form::label('coffin', 'Ataud') !!}
            {!! Form::text('coffin', (isset($service->coffin)) ? $service->coffin : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
        </div>	
    </div>
</div>
<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">  Complementaria 2</h3>
        </div>
		<div class="form-group">
            {!! Form::label('cemetery_id', 'Cementerio') !!}
            {!! Form::select('cemetery_id', $cementerio, (isset($service->cemetery_id)) ? $service->cemetery_id : null, ['class' => 'form-control select2', ''])!!}
        </div>
		<div class="form-group">
			{!! Form::label('comments', 'Observacion') !!}
			{!! Form::textarea('comments', (isset($service->comments)) ? $service->comments : null, ['class' => 'form-control' , 'placeholder' => 'Ingrese Direccion Instalacion' , 'rows' => 3, 'cols' => 40 , 'placeholder' => '', 'required']) !!}	 
        </div>				 
    </div>
</div>
<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">  Datos de Capilla</h3>
        </div>
		<div class="form-group">
            {!! Form::label('chapel_model', 'Modelo Capilla') !!}
            {!! Form::select('chapel_model', $modelocap, (isset($service->chapel_model)) ? $service->chapel_model : null, ['class' => 'form-control select2', ''])!!}
        </div>
		<div class="form-group">
			{!! Form::label('color_cloak_rug', 'Color Manto y Alfombra') !!}
			{!! Form::textarea('color_cloak_rug', (isset($service->color_cloak_rug)) ? $service->color_cloak_rug : null, ['class' => 'form-control' , 'placeholder' => 'Ingrese Direccion Instalacion' , 'rows' => 3, 'cols' => 40 , 'placeholder' => '', 'required']) !!}	 
        </div>			 
    </div>
</div>