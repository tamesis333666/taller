@extends('layouts.admin')

@section('title', 'Listado de Sucursal')

@section('contenido')
    <div class="box">
        @include('util.success')
        <div class="box-header with-border">
            <h3 class="box-title">
                Listado de Citas para Recepcionar
            </h3>
            <div class="box-tools">
				<div class="text-center">
                      
                </div>	
				                    
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box-body table-responsive_ no-padding">
                        <table class="table table-hover display_ table-responsive table-condensed" id="table">
                            <thead>
                            <tr> 
                                <th>CLIENTE</th>
								<th>PLACA</th>
								 
								<th>FEC. CITA</th>
 								<th>ESTADO</th> 
                                <th>ACCIONES</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($datos as $var)
								@php
                                    $fsistema = date('Y-m-d');
                                    $ftermino = date($var->datetime_reservation);
                                    $datetime1 = date_create($fsistema);
                                    $datetime2 = date_create($ftermino);
                                     
                                @endphp
                                @if($datetime2 == $datetime1)
                                    <!-- rojo -->
                                    <tr style="color: #E21C1F;">
                                    
                                @else 
                                    <tr>
                                @endif
								 
									<td>  {{ $var->cliente->full_name }} </td>
									<td> {{ $var->vehiculo->placa }} </td>
 									<td> {{ $var->datetime_reservation }} </td>
									<td> <span class="success">{{ $var->status }}</span> </td>									  
                                    <td>                                        
                                        @if ($var->status != "Anulado")
                                        
                                         <a href="{{ route('fotocar.show', $var->id) }}"  ><i class="fa fa-file" aria-hidden="true"></i>
											 
										</a>
										@endif
									 
										
										<a href="{{ route('cita_customer', [$var->id]) }}"
                                            target="_blank">
                                            <i class="fa fa-print" aria-hidden="true"   ></i>
                                        </a>
										
                                    </td>
 
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <!-- /.box -->
    </div>
@endsection

@section('js')
    <!-- SweetAlert 2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#table').DataTable({
                "order": [[ 1, "desc" ]],
                "language": {
                    "url": "{{ asset('AdminLTE/plugins/datatables/esp.lang') }}"
                }
            });
            
            //Promesa para anular el registro
            const _delete = function (id) {
                return new Promise( (resolve, reject) => {
                    $.ajax({
                        url: '/appointment/servicio/delete',
                        method: 'post',
                        dataType: 'json',
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                        },
                        data: {id: id},
                    }).done(resolve).fail(reject);
                });
            }

            $('.delete_service').on('click', function (e) {
                e.preventDefault();
                let _id = $(this).data('id');                
                Swal.fire({
                    title: 'Estas seguro de anular el registro?',
                    text: "¡No podrás revertir esto!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, anular!',
                    cancelButtonText: 'Cancelar',
                    }).then((result) => {
                    if (result.value) {                       
                        _delete(_id).then(result => {
                            if (result.status) {
                                Swal.fire({
                                    title: 'Anulado!',
                                    text: result.message,
                                    icon: 'success'
                                }).then( () => {
                                    location.reload();
                                });
                            } else {
                                Swal.fire({
                                    title: 'Detalle encontrado!',
                                    text: result.message,
                                    icon: result.type
                                }); 
                            }
                        }).catch(error => {
                            console.log(error)
                        });
                    }
                });
            })
        });
    </script>
@endsection