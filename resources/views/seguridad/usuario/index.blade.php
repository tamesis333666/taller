@extends('layouts.admin')

@section('title', 'Listado de usuarios')

@section('contenido')
    <div class="box">
         <div class="box-header with-border">
            <h3 class="box-title">
                Listado de Usuarios
            </h3>
            <div class="box-tools">
                <div class="text-center">
                     
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover display table-responsive table-condensed" id="table">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Email</th>
                                     
                                    <th>Efectivo Hasta</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>

                            <tbody>
                            @foreach ($usuarios as $usu)
                                <tr>
                                    <td>{{ $usu->id}}</td>
                                    <td>{{ $usu->name}}</td>
                                    <td>{{ $usu->email}}</td>
                                     
                                    <td>{{ 
                                      \Carbon\Carbon::parse($usu->effective_end_date)->format('d/m/Y') }}</td>
                                    <td>  
                                        <a href="{{URL::action('UsuarioController@edit',$usu->id)}}">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>
                                          
                                    </td>
                                    
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <!-- footer-->
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#table').DataTable({
                "language": {
                    "url": "{{ asset('AdminLTE/plugins/datatables/esp.lang') }}"
                }
            });
        });
    </script>
@endsection