@extends('layouts.admin')

@section('title', 'Listado de Clientes')
 
@section('contenido')
    <div class="box">
         <div class="box-header with-border">
            <h3 class="box-title">
                Listado de Clienteseee
            </h3>
            <div class="box-tools">
                <div class="text-center">
                    
                     
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover display table-responsive table-condensed" id="table">
                            <thead>
                            <tr>
                                <th>NOMBRE COMPLETO</th>
                                <th>DOCUMENTO</th>
                                <th>CONTACTO</th>
                               
                                <th>TELEFONO MOVIL</th>
                             </tr>
                            </thead>
                            <tbody>
                            @foreach($rsclientes as $person)
                                <tr>
                                    <td>
                                        {{ $person->full_name }}
                                    </td>
                                    <td>
                                        {{ $person->num_documento }}
                                    </td>
                                    <td>
                                        {{ $person->contacto }}
                                    </td>
                                     
                                    <!-- PARA MOSTRAR FECHAS
                                        {{ \Carbon\Carbon::parse($person->effective_end_date)->format('d/m/Y') }}
                                    </td>  retonar en pantalla formato=> 03/05/2050
                                    
                                    <td>
                                        {{ $person->full_name }}
                                    </td>
                                    <td>
                                        {{ $person->date_of_birth }}
                                    </td>  retonar en pantalla formato=> 13-05-2017
                                    -->
                                    <td>
                                        {{ $person->telef2 }}
                                    </td>
									 
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <!-- footer-->
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->
    </div>
@endsection

@section('js')
 

    <script type="text/javascript">
        $(document).ready(function () {
            $('#table').DataTable({
                "language": {
                    "url": "{{ asset('AdminLTE/plugins/datatables/esp.lang') }}"
                }
            });
        });
    </script>
@endsection