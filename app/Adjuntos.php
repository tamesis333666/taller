<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class adjuntos extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];
    protected $table = 'adjuntos';
    protected $fillable = [ 'module','description','file_name','file_patch','type','file_id'];
}
