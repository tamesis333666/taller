<?php

namespace sisVentas;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;
use DB;

class Cliente extends Model
{
    protected $table = 'cliente';

    public $timestamps= true;

    protected $fillable = ['idcliente', 'first_name', 'second_name', 'first_last_name', 'second_last_name', 'site_id',
    'full_name', 'date_of_birth', 'effective_end_date','sex', 'tipo_documento', 'num_documento',
        'email_address', 'telef1', 'telef2',  'disccount', 'country', 'address',
        'created_by', 'last_updated_by','updated_at','created_at',
        'contacto','no_atender','motivo_no_atencion'];

    protected $primaryKey = 'idcliente';

  public function vehiculos()
    {
        return $this->hasMany('sisVentas\Vehiculo');
    }
    
//CODIGO PARA RELACIONAR CON LAS TABLAS

    public function tipo_doc()
    {
        return $this->hasOne('App\Tipo_docs');
    }
 
 
public function getDATEOFBIRTHAttribute($date)
{
    if($date != '0000-00-00'){
    return $date = \Carbon\Carbon::parse($date)->format('d-m-Y');
    }
    return '';
}

public function setDATEOFBIRTHAttribute($date)
    {
        if($date == ''){
            $this->attributes['date_of_birth'] = \Carbon\Carbon::parse('0000-00-00')->format('Y-m-d');
        }else{
            $this->attributes['date_of_birth'] = \Carbon\Carbon::parse($date)->format('Y-m-d');
        }
    }
        


public function getEFFECTIVEENDDATEAttribute($date)
{
   return $date = \Carbon\Carbon::parse($date)->format('d-m-Y');
}


public function setEFFECTIVEENDDATEAttribute($date)
{
   $this->attributes['effective_end_date'] = \Carbon\Carbon::parse($date)->format('Y-m-d');
}


public function getDniAttribute()
{
    return $this->num_documento;
}


	public function ordentrabajo()
    {
        return $this->hasMany(WorkOrder::class,'owner_id','idcliente')->orderBy('created_at','desc');
    }

            
       public function mano_obraActivo_completed()
    { return $this->hasMany(WorkOrder::class,'owner_id','idcliente')
            ->where('eam_work_resource.enabled','=',1)
    ->orderBy('created_at','desc');

        return $this->hasMany(EamResource::class,'wip_entity_id','wip_entity_id')
                ->where('eam_work_resource.enabled','=',1)
                ->where('eam_work_resource.completed','=',1);
    }

}
