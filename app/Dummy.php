<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dummy extends Model
{ 
	 
    protected $table = 'Dummy';
    protected $fillable = [  'c1', 'c2'];
}
