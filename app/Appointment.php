<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;

class Appointment extends Model
{

    protected $table = "mkt_reservations";

    protected $fillable = [  
  	   'id', 'numcita',
  	   'placa_id', 
  	   'customer_id',
       'date_reservation',
       'datetime_reservation',
       'site_id',
       'confirmed',
       'service1_id',
       'service2_id',
       'comments',
       'status',
        'updated_at',
       'last_updated_by',         
       'created_by',
       'created_at', 
         
	   ];

      public function attachedDocuments()
    {
      return $this->hasMany(FndAttachedDocuments::class, 'pk1_value');
    }
    
        public function sucursal()
    {
        return $this->hasOne(Site::class,'id','site_id');
    }
    
    public function cliente()
    {
        return $this->belongsTo('sisVentas\Cliente', 'customer_id', 'idcliente');
    }

    public function servicio1()
    {
        return $this->belongsTo('sisVentas\Labor', 'service2_id', 'idlabor');
    }


    public function servicio2()
    {
        return $this->belongsTo('sisVentas\Labor', 'service1_id', 'idlabor');
    }


  public function vehiculo()
    {
        return $this->hasOne(Vehiculo::class,'id','placa_id');
    }

   

    public function marca()
    {
        return $this->belongsTo('sisVentas\Marca', 'idmarca', 'idmarca');
    }

    public function user()
    {
        return $this->belongsTo('sisVentas\User', 'last_updated_by');
    }

    public function createby()
    {
        return $this->belongsTo('sisVentas\User', 'created_by');
    }
 
    
}