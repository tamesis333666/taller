<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $table = 'inv_sub_inventory';

    protected $fillable = ['name', 'site_id'];

    public function site()
    {
    	return $this->belongsTo(Site::class);
    }
}
