<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;
use sisVentas\User;
class UserPermisos extends Model
{
    protected $table = 'users_permisos';

    protected $fillable = [
        'id_user',
        'permisos',
    ];

    public $timestamps = true;


    public function users()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
}
