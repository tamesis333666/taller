<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class FndAttachedDocuments extends Model
{
  protected $table = "fnd_attached_documents";

  protected $primaryKey = 'attached_document_id';

  public $timestamps=true;

  protected $fillable = ['entity_name','pk1_value','name_file','seq_num','path_file','description','created_by','last_updated_by'];

  public function workOrder()
  {
      return $this->belongsTo('sisVentas\WorkOrder','pk1_value');
  }

}

