<?php

namespace sisVentas;
use Carbon\Carbon;
use DB;

use Illuminate\Database\Eloquent\Model;

class AsCotizacion extends Model
{
    protected $table = 'as_cotizacion';
 
    protected $fillable = [
        'nrocotizac',
        'site_id',
        'Idcliente',
        'placa',
        'idasesor',
        'id_use_vehicle',
        'dscto',
		'comments',
        'totalrep',
        'totalmo',
        'subtotal',
        'igv',
        'due_date',
        'total',
        'tipocot',
        'updated_at',
        'created_at',
        'last_updated_byc'
    ];

    public function sucursal()
    {
        return $this->hasOne(Site::class,'id','site_id');
    }

    public function repuestos()
    {
        return $this->hasMany(AsCotizaRep::class,'idcotiza','Idcotiza');
    }

    public function repuestos_manual()
    {
        return $this->hasMany(AsCotizaManRep::class,'idcotiza','Idcotiza')->orderBy( 'orden', 'ASC' );
    }

    public function mano_obra()
    {
        return $this->hasMany(AsCotizaMo::class,'idcotiza','Idcotiza');
    }

    public function mano_obra_manual()
    {
        return $this->hasMany(AsCotizaManMo::class,'idcotiza','Idcotiza')->orderBy( 'orden', 'ASC' );
    }

    public function cliente()
    {
        return $this->hasOne(Cliente::class,'idcliente','Idcliente');
    }

    public function vehiculo()
    {
        return $this->hasOne(Vehiculo::class,'placa','placa');
    }

        public function asesor()
    {
        return $this->hasOne(Personal::class,'PERSON_ID','idasesor');
    }

    public function totalLinea2()
    { 
        return $this->hasMany(AsCotizaRep::class,'idcotiza','Idcotiza')->select(DB::raw('as_cotizarep.cantidad * format( (as_cotizarep.precio/1.18),2) as total'));
    }

    public function totalLinea()
    { 
        return $this->hasMany(AsCotizaRep::class,'idcotiza','Idcotiza')->select(DB::raw('as_cotizarep.cantidad *  as_cotizarep.precio as total'));
    }

     public function totalLineaRep()
    { 
        return $this->hasMany(AsCotizaRep::class,'idcotiza','Idcotiza')->select(DB::raw('as_cotizarep.cantidad *  as_cotizarep.precio as total'));
    }

    public function totalLineaTax()
    { 
        return $this->hasMany(AsCotizaRep::class,'idcotiza','Idcotiza')->select(DB::raw('as_cotizarep.precio - format( (as_cotizarep.precio/1.18),2) as total'));
    }

    public function totalLineaMan()
    { 
        return $this->hasMany(AsCotizaManRep::class,'idcotiza','Idcotiza')->select(DB::raw('as_cotizamanrep.cantidad * as_cotizamanrep.costo  as total'));
    }

        public function totalLineaManRep()
    { 
        return $this->hasMany(AsCotizaManRep::class,'idcotiza','Idcotiza')->select(DB::raw('as_cotizamanrep.cantidad * as_cotizamanrep.costo  as total'));
    }

    public function totalLineaManTax()
    { 
        return $this->hasMany(AsCotizaManRep::class,'idcotiza','Idcotiza')->select(DB::raw('as_cotizamanrep.costo - format( (as_cotizamanrep.costo/1.18),2) as total'));
    }    
 
     public function totalLineaLab()
    { 
        return $this->hasMany(AsCotizaMo::class,'idcotiza','Idcotiza')->select(DB::raw('  as_cotizamo.costolabor as total'));
    }

     public function totalLineaLabTax()
    { 
        return $this->hasMany(AsCotizaMo::class,'idcotiza','Idcotiza')->select(DB::raw('  as_cotizamo.costolabor - format( (as_cotizamo.costolabor/1.18),2) as total'));
    }

    public function totalLineaLabMan()
    { 
        return $this->hasMany(AsCotizaManMo::class,'idcotiza','Idcotiza')->select(DB::raw('  as_cotizamanmo.costo as total'));
    }   

    public function totalLineaLabManTax()
    { 
        return $this->hasMany(AsCotizaManMo::class,'idcotiza','Idcotiza')->select(DB::raw('  as_cotizamanmo.costo - format( (as_cotizamanmo.costo/1.18),2) as total'));
    }    


    public function scopeSearchLines($query, $date)
    {
        $placa=array_get($date, 'placa', false);
        $sucursal=array_get($date, 'sucursal', false);
        $numot=array_get($date, 'numot', false);
        $numcotiz=array_get($date, 'numcotiz', false);
        $idcliente=array_get($date, 'idcliente', false);
        $PERSON_ID=array_get($date, 'PERSON_ID', false);
        
        return $query
            ->join('inv_sites', 'inv_sites.id', '=', 'as_cotizacion.site_id')
            ->join('cliente', 'cliente.idcliente', '=', 'as_cotizacion.Idcliente')
            ->join('hr_per_people_inf as h', 'h.PERSON_ID', '=', 'as_cotizacion.idasesor')
            ->leftjoin('as_cotizamanmo as b', 'b.idcotiza', '=', 'as_cotizacion.Idcotiza')
            ->leftjoin('as_cotizamanrep as e', 'e.idcotiza', '=', 'as_cotizacion.Idcotiza')
            ->leftjoin('as_cotizamo as f', 'f.idcotiza', '=', 'as_cotizacion.Idcotiza')
            ->leftjoin('as_cotizarep as g', 'g.idcotiza', '=', 'as_cotizacion.Idcotiza')

            ->when($placa, function ($query) use ($placa) {
                return $query->where('as_cotizacion.placa', $placa);
            })
            ->when($numot, function ($query) use ($numot) {
                return $query->where('as_cotizacion.num_ot', $numot);
            })
            ->when($sucursal, function ($query) use ($sucursal) {
                return $query->where('inv_sites.id', $sucursal);
            })
            ->when($numcotiz, function ($query) use ($numcotiz) {
                return $query->where('as_cotizacion.numcotiz', $numcotiz);
            })
            ->when($idcliente, function ($query) use ($idcliente) {
                return $query->where('cliente.idcliente', $idcliente);
            })
            ->when($PERSON_ID, function ($query) use ($PERSON_ID) {
                return $query->where('hr_per_people_inf.PERSON_ID', $PERSON_ID);
            })
            ->select('as_cotizacion.Idcotiza', 'inv_sites.name as sucursal', 'as_cotizacion.nrocotizac as nrocotizac', 'cliente.full_name as cliente', 'cliente.telef2 as telefono', 'as_cotizacion.placa', 'h.FULL_NAME as asesor', 'as_cotizacion.created_at as fec_creacion', 'as_cotizacion.num_ot as num_ot', 'as_cotizacion.Idcotiza as idcotiza','f.descripcion as mano_obra_lista', 'f.costolabor as mano_obra_lista_costo', 'e.repuesto as repuesto_manual_descr', 'e.costo as repuesto_manual_costo', 'e.cantidad as repuesto_manual_cantidad', 'b.manoobra as mano_obra_manual_descr',  'b.costo as mano_obra_manual_costo','g.descripcion as repuesto_lista', 'g.cantidad as repuesto_lista_cantidad','g.precio as repuesto_lista_precio');
 
    }


    public function scopeSearch($query, $date)
    {
        $placa=array_get($date, 'placa', false);
        $sucursal=array_get($date, 'sucursal', false);
        $numot=array_get($date, 'numot', false);
        $numcotiz=array_get($date, 'numcotiz', false);
        $idcliente=array_get($date, 'idcliente', false);
        $PERSON_ID=array_get($date, 'PERSON_ID', false);
        
        return $query
            ->join('inv_sites', 'inv_sites.id', '=', 'as_cotizacion.site_id')
            ->join('cliente', 'cliente.idcliente', '=', 'as_cotizacion.Idcliente')
            ->join('hr_per_people_inf', 'hr_per_people_inf.PERSON_ID', '=', 'as_cotizacion.idasesor')
            ->leftjoin('as_cotizamanmo as b', 'b.idcotiza', '=', 'as_cotizacion.Idcotiza')
            ->leftjoin('as_cotizamanrep as e', 'e.idcotiza', '=', 'as_cotizacion.Idcotiza')
            ->leftjoin('as_cotizamo as f', 'f.idcotiza', '=', 'as_cotizacion.Idcotiza')
            ->leftjoin('as_cotizarep as g', 'g.idcotiza', '=', 'as_cotizacion.Idcotiza')
            ->when($placa, function ($query) use ($placa) {
                return $query->where('as_cotizacion.placa', $placa);
            })
            ->when($numot, function ($query) use ($numot) {
                return $query->where('as_cotizacion.num_ot', $numot);
            })
            ->when($sucursal, function ($query) use ($sucursal) {
                return $query->where('inv_sites.id', $sucursal);
            })
            ->when($numcotiz, function ($query) use ($numcotiz) {
                return $query->where('as_cotizacion.numcotiz', $numcotiz);
            })
            ->when($idcliente, function ($query) use ($idcliente) {
                return $query->where('cliente.idcliente', $idcliente);
            })
            ->when($PERSON_ID, function ($query) use ($PERSON_ID) {
                return $query->where('hr_per_people_inf.PERSON_ID', $PERSON_ID);
            })
            ->select('as_cotizacion.Idcotiza', 'as_cotizacion.num_ot', 'inv_sites.name as sucursal', 'as_cotizacion.nrocotizac as nrocotizac', 'cliente.full_name as cliente', 'cliente.telef2 as telefono', 'as_cotizacion.placa', 'hr_per_people_inf.FULL_NAME as asesor', 'as_cotizacion.created_at as fec_creacion', 'as_cotizacion.num_ot as num_ot', 'as_cotizacion.Idcotiza as idcotiza', 
                     DB::raw('SUM(b.costo) as tot_manmo'),
                     DB::raw('SUM(e.costo * e.cantidad) as tot_manrep'),
                     DB::raw('SUM(f.costolabor) as tot_mo'),
                     DB::raw('SUM(g.precio * g.cantidad) as tot_rep' )
                 )
            ->groupBy('as_cotizacion.Idcotiza');
 
    }

}
