<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
 

Route::get('/', function () {
    return view('auth/login');
});


//RUTA PARA LA GUARDAR FOTOS DEL VEHICULO  
Route::resource('fotocar','FotoCarController');

Route::post('/subir/{id}/{placa}/{cliente}', 'FotoCarController@subir')->name('fotocar.subir');

Route::get('/acerca', function () {
    return view('acerca');
});
 
 
/*LOGIN // NUEVO CLIENTE FRONTEND*/
//Route::get('ventas/creacion', 'Ventas2Controller@create')->name('ventas.create');
Route::get('front/nuevo-cliente', 'FrontEndController@create')->name('nuevo.cliente');
Route::post('/store/sales', 'SalesOrderController@store');

//FIN LOGIN
 
// 

//MODULO DE GESTION DE CITAS ATENCION VEHICULAR
 
/*Ruta para guardar la recepcion de cita */
Route::resource('appointment/receipt','AppointmentController@receipt_cita');
 
 
Route::get('mkt/{cita_id}/pdf', 'AppointmentController@cita_cliente')->name('cita_customer');
 
//FIN MODULO GESTION DE CITAS ATENCION VEHICULAR

// 
//ASESOR : RECEPCION
//Route::resource('asesor/inventario','RecepcionAsesorController');
 
  
Route::auth();

   

/**--------- reportes */
 


// ----------------------------------------------------------------------------------


//FIN RECEPCION OC

 
//fin PARAMETROS  DE SUCURSAL


// Inicio Seguridad-Sucursal
  
//++INICIO RECEPCIONES
 
//FIN RECEPCIONES

 
/*** RUTAS PARA COTIZACIÓN*/
 

//Vehiculos - RT 
//FIN Tipo de Transacciones
 
//FIN TRX VARIAS
 
// FIN ITEM-MMA

//IMPORTACION DE REGISTROS
 
//FIN //IMPORTACION DE REGISTROS


// organizacion

Route::get('/home', 'HomeController@index');
 
Route::get('/home', 'HomeController@index');


// fndlookup 
 
// labor
 
Route::get('/{slug?}', 'HomeController@index');
 
//CREACION AUTOMATICA DE ORDEN DE TRABAJO
Route::resource('asesor/vehiculo','VehiculoController');


Route::resource('ventas/cliente','ClienteController');
 
Route::resource('seguridad/usuario','UsuarioController');
 
Route::resource('appointment/receipt','AppointmentController@receipt_cita');

Route::get('/subir', function(){
	return "hola mundo";
});



  