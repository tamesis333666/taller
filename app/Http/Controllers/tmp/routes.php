<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('auth/login');
});
Route::get('/acerca', function () {
    return view('acerca');
});

Route::resource('almacen/categoria','CategoriaController');
Route::resource('almacen/articulo','ItemController'); //'ArticuloController');
Route::resource('almacen/marca','MarcaController');
Route::resource('almacen/familia','FamiliaController');
Route::resource('almacen/modelo','ModeloController');
Route::resource('almacen/labor','LaborController');
Route::resource('asesor/vehiculo','VehiculoController');
Route::resource('ventas/cliente','ClienteController');
Route::resource('ventas/venta','VentaController');
Route::resource('compras/proveedor','ProveedorController');
Route::resource('compras/ingreso','IngresoController');
Route::resource('seguridad/usuario','UsuarioController');
Route::resource('seguridad/forms','PantallaController');
Route::resource('hr/position','PositionController');
Route::resource('configuracion/uom','UomController');
Route::resource('configuracion/currency','FndCurrencyController');
Route::resource('cg/dailyrate','DailyRateController');
Route::resource('cg/convertion-type','ConversionTypeController');
Route::resource('po/buyer','BuyerController');

Route::auth();


/*** RUTAS PARA COTIZACIÓN*/
Route::group(['prefix' => 'asesor'], function () {
    Route::group(['prefix' => 'cotizacion'], function () {


        Route::get('/',  'CotizacionController@index');
        Route::get('/nueva',  'CotizacionController@frm');
        Route::get('/editar/{id?}',  'CotizacionController@frm');
        Route::get('/imprimir/{id?}',  'CotizacionController@imprimir');
        Route::get('/correo/{id?}',  'CotizacionController@correo');


        Route::post('/guardar',  'CotizacionController@create');
        Route::post('/editar',  'CotizacionController@update');


        Route::post('/clientes',  'CotizacionController@cat_clientes');
        Route::post('/vehiculo',  'CotizacionController@cat_vehiculo');
        Route::post('/repuestos',  'CotizacionController@cat_repuestos');
        Route::post('/mano-obra',  'CotizacionController@cat_mano_obra');

        Route::get('/mail/{id?}',  'CotizacionController@envioCorreo');

    });
});
/***FIN RUTAS PARA COTIZACIÓN*/
 
// personal - RT
Route::group(['prefix' => 'hr'], function () {
    Route::post('/personal/{flag}', 'PersonalController@index')->name('all');
    Route::resource('personal', 'PersonalController');
});

Route::get('excel',  'PersonalController@export')->name('export');


//Vehiculos - RT
Route::group(['prefix' => 'asesor'], function () {
    Route::resource('vehiculo', 'VehiculoController');
    
    //Consulta Vehiculos - RT 27.05.2017
    Route::get('search', [
        'uses' => 'VehiculoController@search',
        'as' => 'search'
    ]);

    Route::get('query/query', [
        'uses' => 'VehiculoController@query',
        'as' => 'query'
    ]);
    //FIN Consulta Vehiculos
});
Route::post('select-ajax', ['as'=>'select-ajax','uses'=>'VehiculoController@selectAjax']);
Route::get('excelvehiculos',  'VehiculoController@export')->name('exportvehiculos');


Route::get('excelquery',  'VehiculoController@exportquery')->name('exportquery');
//FIN Vehiculos

 

//Unidades de Medida - Definicion Clase UOM
Route::group(['prefix' => 'configuracion'], function () {
    Route::resource('claseuom', 'UomClaseController');
});

//FIN Clase - Unidades de Medida

// LOCALIZADOR 
Route::group(['prefix' => 'configuracion'], function () {
    Route::post('/localizador}', 'LocatorController@index')->name('all');
    Route::resource('localizador', 'LocatorController');
});

Route::get('excel',  'PersonalController@export')->name('export');
//--FIN LOCALIZADOR

//Item
Route::group(['prefix' => 'almacen'], function () {
    Route::resource('item', 'ItemController');
    //Route::post('/item/items',  function(){return 'hola';
    Route::post('/item/items','ItemController@cat_item' ) ;
    
});


Route::get('excelitems',  'ItemController@export')->name('exportitems');
//FIN Item

//Direcciones
Route::group(['prefix' => 'configuracion'], function () {
    Route::resource('direccion', 'DireccionController');
});

Route::get('excelitems',  'ItemController@export')->name('exportitems');
//FIN Direcciones

//Tipo de Transacciones
Route::group(['prefix' => 'configuracion'], function () {
    Route::resource('tipotrx', 'TipoTrxController');
});
 
Route::get('excelvehiculos',  'VehiculoController@export')->name('exportvehiculos');
//FIN Tipo de Transacciones

// Cliente
Route::group(['prefix' => 'ventas'], function () {
    Route::post('/cliente/{flag}', 'ClienteController@index')->name('all');
    Route::resource('cliente', 'ClienteController');
});
Route::get('excel2',  'ClienteController@export')->name('exportCliente');
// FIN Cliente

// proveedor
Route::group(['prefix' => 'compras'], function () {
    Route::post('/proveedor/{flag}', 'ProveedorController@index')->name('all');
    Route::resource('proveedor', 'ProveedorController');
});
Route::get('excel3',  'ProveedorController@export')->name('exportvendor');
// FIN proveedor


//TRANSACCIONES VARIAS - yasser
Route::get('almacen/transactions/crear', ['uses' => 'InvMaterialTrxController@create', 'as' => 'transaction.create']);
Route::get('almacen/transactions', ['uses' => 'InvMaterialTrxController@index', 'as' => 'transaction.index']);
Route::get('almacen/transactions/show', ['uses' => 'InvMaterialTrxController@show', 'as' => 'transaction.show']);
Route::post('item-type/get-all', 'TipoTrxController@getAll');
Route::post('almacen/item/get-all', 'ItemController@getAll');
Route::post('almacen/sub-inventarios/get-all', 'SubInventarioController@getAll');
Route::post('almacen/locator/get-all', 'LocatorController@getAll');
Route::post('almacen/uom/get-all', 'UomController@getAll');
Route::post('almacen/fnd-lookup-value/get-all', 'FndLookupValueController@getAll');
Route::post('almacen/transactions', 'InvMaterialTrxController@store');

//FIN TRX VARIAS


//ITEM MMA - yasser
Route::get('almacen/itemMMA', ['uses' => 'ItemMmaController@index', 'as' => 'itemmma']);
Route::get('almacen/itemMMA/crear', ['uses' => 'ItemMmaController@create', 'as' => 'itemmma.create']);
Route::post('almacen/itemMMA', ['uses' => 'ItemMmaController@store', 'as' => 'itemmma.store']);
Route::get('almacen/itemMMA/{itemmma}/edit', ['uses' => 'ItemMmaController@edit', 'as' => 'itemmma.edit']);
Route::put('almacen/itemMMA/{itemmma}', ['uses' => 'ItemMmaController@update', 'as' => 'itemmma.update']);
Route::delete('almacen/itemMMA/{itemmma}', ['uses' => 'ItemMmaController@destroy', 'as' => 'itemmma.delete']);
Route::post('get-modelos-marca', ['uses' => 'ModeloController@getModelos', 'as' => 'getmodelos' ]);

Route::post('get-uomcode', ['uses' => 'ItemController@getuomcode', 'as' => 'getuomcode' ]);

Route::group(['prefix' => 'almacen'], function () { 
      
    //Consulta Articulos-MMA - 04.06.2017
    Route::group(['prefix' => 'almacen'], function () {
        Route::resource('ItemMMA', 'ItemMmaController');

        Route::get('searchitemMMA', [
            'uses' => 'ItemMmaController@searchitemMMA',
            'as' => 'searchitemMMA'
        ]);

        Route::get('query/query', [
            'uses' => 'ItemMmaController@queryItemMMA',
            'as' => 'queryItemMMA'
        ]);
    });

    Route::get('excelqueryMMA',  'ItemMmaController@exportqueryMMA')->name('exportqueryMMA');
    //FIN Consulta Articulos MMA

});

// FIN ITEM-MMA


// organizacion

Route::get('/home', 'HomeController@index');

Route::resource('organizacion', 'OrganizacionController');

Route::get('configuracion/organizacion', 'OrganizacionController@index');

Route::get('configuracion/organizacion/create', 'OrganizacionController@create');

Route::post('configuracion/organizacion/store', 'OrganizacionController@store');

Route::post('configuracion/organizacion/update', 'OrganizacionController@update');

Route::get('/reporteorganizacion', 'OrganizacionController@reporte');

Route::get('/home', 'HomeController@index');


// fndlookup

Route::resource('fndlookup', 'FndLookupController');

Route::get('configuracion/fndlookup', 'FndLookupController@index');

Route::get('configuracion/fndlookup/create', 'FndLookupController@create');

Route::post('configuracion/fndlookup/store', 'FndLookupController@store');

Route::post('configuracion/fndlookup/update', 'FndLookupController@update');

Route::get('configuracion/destroy_fndlookup/{id}', 'FndLookupController@destroy');

Route::get('/activar_fndlookup/{id}', 'FndLookupController@activar');

Route::get('/reportefndlookup', 'FndLookupController@reporte');

Route::get('/ajax_edit_lookup', 'FndLookupValueController@ajax_edit_lookup');


// labor

Route::resource('labor', 'LaborController');

Route::get('almacen/labor', 'LaborController@index');

Route::get('almacen/labor/create', 'LaborController@create');

Route::post('almacen/labor/store', 'LaborController@store');

Route::post('almacen/labor/update', 'LaborController@update');

Route::get('almacen/destroy_labor/{id}', 'LaborController@destroy');

Route::get('almacen/activar_labor/{id}', 'LaborController@activar');

Route::get('reportelabor', 'LaborController@reporte');

Route::get('ajax_edit_labor', 'LabormmaController@ajax_edit_labor');


// subinventario

Route::resource('subinventario', 'SubInventarioController');

Route::get('almacen/subinventario', 'SubInventarioController@index');

Route::get('almacen/subinventario/create', 'SubInventarioController@create');

Route::post('almacen/subinventario/store', 'SubInventarioController@store');

Route::post('almacen/subinventario/update', 'SubInventarioController@update');

Route::get('almacen/destroy_subinventario/{id}', 'SubInventarioController@destroy');

Route::get('almacen/activar_subinventario/{id}', 'SubInventarioController@activar');

Route::get('reportesubinventario', 'SubInventarioController@reporte');

Route::get('/ajax_edit_subinventario', 'SubInventarioController@ajax_edit_subinventario');

// Ajax validacion




Route::get('ajax_modelos', 'LaborController@ajax_modelo');

Route::get('ajax_validacion', 'OrganizacionController@validationall');

Route::get('ajax_validacion_update', 'OrganizacionController@validationallupdate');

Route::get('ajax_validad_marca', 'LaborController@ajax_validad_marca');

Route::get('ajax_guardar_marca', 'LabormmaController@store');

Route::get('eliminar_marca', 'LabormmaController@destroy');

Route::get('/ajax_labor_eliminar', 'LaborController@destroy');

Route::get('/ajax_labor_activar', 'LaborController@activar');

Route::get('/ajax_lookup_eliminar', 'FndLookupController@destroy');

Route::get('/ajax_lookup_activar', 'FndLookupController@activar');

Route::get('/guardar_lookup_value', 'FndLookupValueController@store');

Route::get('/eliminar_lookup_value', 'FndLookupValueController@destroy');

Route::get('/ajax_sub_inventario_destroy', 'SubInventarioController@destroy');

Route::get('/ajax_sub_inventario_activar', 'SubInventarioController@activar');



//Reportes
Route::get('reportecategorias', 'CategoriaController@reporte');
Route::get('reportemarcas', 'MarcaController@reporte');
Route::get('reportemodelos', 'ModeloController@reporte');
Route::get('reportefamilias', 'FamiliaController@reporte');
Route::get('reportearticulos', 'ArticuloController@reporte');
Route::get('reporteclientes', 'ClienteController@reporte');
Route::get('reporteproveedores', 'ProveedorController@reporte');
Route::get('reporteventas', 'VentaController@reporte');
Route::get('reporteventa/{id}', 'VentaController@reportec');
Route::get('reporteingresos', 'IngresoController@reporte'); 
Route::get('reporteingreso/{id}', 'IngresoController@reportec'); 


Route::get('/{slug?}', 'HomeController@index');

//Usado para mostrar el modal de historial del registro
Route::get('/datacategoria/{id}', 'CategoriaController@showDataModal')->name('modal.historycat');
Route::get('/dataitem/{id}', 'ItemController@showDataModal')->name('modal.historyitem');
Route::get('/dataitemmma/{id}', 'ItemMmaController@showDataModal')->name('modal.historyitemmma');
Route::get('/datalabor/{id}', 'LaborController@showDataModal')->name('modal.historylabor');
Route::get('/datamarca/{id}', 'MarcaController@showDataModal')->name('modal.historymarca');
Route::get('/datalocalizador/{id}', 'LocatorController@showDataModal')->name('modal.historylocator');
Route::get('/datamodelo/{id}', 'ModeloController@showDataModal')->name('modal.historymodelo');
Route::get('/datadaily/{id}', 'DailyRateController@showDataModal')->name('modal.historydailyrate');
Route::get('/dataconvert/{id}', 'ConversionTypeController@showDataModal')->name('modal.historyconv-type');
Route::get('/datacli/{id}', 'ClienteController@showDataModal')->name('modal.historycli');
Route::get('/datacurrency/{id}', 'FndCurrencyController@showDataModal')->name('modal.historycurrency');
