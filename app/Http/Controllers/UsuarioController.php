<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use sisVentas\Http\Requests;
use sisVentas\Personal;
use sisVentas\User;
use Illuminate\Support\Facades\Redirect;
use sisVentas\Http\Requests\UsuarioFormRequest;
 
use sisVentas\UserPermisos;
use Laracasts\Flash\Flash;

use DB;

class UsuarioController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {   
        $usuarios = User::get();
        return view('seguridad.usuario.index')->with('usuarios', $usuarios);

    }

    public function create()
    {
        
        $date = Carbon::now()->format('Y-m-d');
        $personal = Personal::orderBy('FULL_NAME', 'ASC')->where('EFFECTIVE_END_DATE', '>=', $date)->lists('FULL_NAME', 'PERSON_ID');
         
        return view("seguridad.usuario.create")->with('rspersonal', $personal);
        
		

    }
    public function store (UsuarioFormRequest $request)
    {
        $usuario=new User;
        $usuario->name=$request->get('name');
        $usuario->email=$request->get('email');
        $usuario->password=bcrypt($request->get('password'));
        $usuario->effective_end_date=$request->get('effective_end_date');
        $usuario->person_id=$request->get('person_id');
        $usuario->save();


         $id = $usuario->id;
        
        $permiso = new UserPermisos();
        $permiso->id_user = $id;
        $permiso->save(); 

        //dd($permiso);
        //return $user_create;
        
        
        Flash::success("Se ha registrado de manera exitosa!")->important();

        return Redirect::to('seguridad/usuario');
    }


    public function edit($id)
    { 
        $usuarios = User::find($id);

        $date = Carbon::now()->format('Y-m-d');
        $personal = Personal::orderBy('FULL_NAME', 'ASC')->where('EFFECTIVE_END_DATE', '>=', $date)->lists('FULL_NAME', 'PERSON_ID');
         
        return view("seguridad.usuario.edit")->with('usuarios', $usuarios)->with('rspersonal', $personal);

        //return view("seguridad.usuario.edit",["usuario"=>User::findOrFail($id)]);
    }    

    public function changepwd($id)
    { 
        $usuarios = User::find($id);

        $date = Carbon::now()->format('Y-m-d');
        $personal = Personal::orderBy('FULL_NAME', 'ASC')->where('EFFECTIVE_END_DATE', '>=', $date)->lists('FULL_NAME', 'PERSON_ID');
         
        return view("seguridad.usuario.changepwd")->with('usuarios', $usuarios)->with('rspersonal', $personal);

        //return view("seguridad.usuario.edit",["usuario"=>User::findOrFail($id)]);

    }   

    public function update(UsuarioFormRequest $request,$id)
    {
        $usuario=User::findOrFail($id);
        $usuario->name=$request->get('name');
        $oper =$request->get('tipodocumento');
        $usuario->person_id=$request->get('person_id');
        $usuario->email=$request->get('email');
        $usuario->password=bcrypt($request->get('password'));
        $usuario->update();

        
        
        if ($oper == 2 )
        {     $oper = 2;
            Flash::success("Se actualizo la clave con exito!")->important();
            return redirect()->back();
            }
        else   
        { 
            Flash::success("El usuario ha sido editado con exito!")->important();
            return Redirect::to('seguridad/usuario');
        }
    }
     
    public function updatepwd(UsuarioFormRequest $request,$id)
    {
        $usuario=User::findOrFail($id);
        $usuario->name=$request->get('name');
        $usuario->person_id=$request->get('person_id');
        $usuario->email=$request->get('email');
        $usuario->password=bcrypt($request->get('password'));
        $usuario->update();

        Flash::success("El usuario ha sido editado con exito!")->important();
        
        
    }
    public function destroy($id)
    {
        $usuario = DB::table('users')->where('id', '=', $id)->delete();
        return Redirect::to('seguridad/usuario');
    }
}
