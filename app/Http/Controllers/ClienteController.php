<?php

namespace sisVentas\Http\Controllers;
use Illuminate\Http\Request;

use sisVentas\Http\Requests;
use sisVentas\Cliente;
use sisVentas\Factura;

use sisVentas\Http\Requests\ClienteFormRequest;

use Carbon\Carbon;
use sisVentas\Tipo_docs;

use Laracasts\Flash\Flash;
use Maatwebsite\Excel\Facades\Excel;

use Auth;
use DB;
use Fpdf;

class ClienteController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }


     public function index($flag = null)
    {
        $date = Carbon::now()->format('Y-m-d');

            $cliente = Cliente::orderBy('first_last_name', 'ASC')->get();

        /*
        if ($flag) {
            $cliente = Cliente::orderBy('first_last_name', 'ASC')->get();
        } else {
            $cliente = Cliente::orderBy('first_last_name', 'ASC')->where('effective_end_date', '>=', $date)->get();
        }
        return view('ventas.cliente.index')->with('rsclientes', $cliente)->with('flag',$flag);

        */
        return view('ventas.cliente.index')->with('rsclientes', $cliente) ;
    }


    public function create()
    {
        $tipo_docs = Tipo_docs::orderBy('nombre', 'ASC')->lists('nombre', 'idtipo_doc');

        return view('ventas.cliente.create')->with('tipo_docs', $tipo_docs);
    }

    public function store(ClienteFormRequest $request)
    {   
        dd( $request ) ;
        $request = $request->all();


        $request['created_by'] = Auth()->user()->id;
         $request['effective_end_date'] ='01-01-2099';
        $request['last_updated_by'] = Auth()->user()->id;

        $request['full_name'] = $request['first_last_name'] . " " . $request['second_last_name'] . " " . $request['first_name'] . " " . $request['second_name'];

       // dd( $request);
        $cliente = new Cliente($request);
        $cliente->save();

        Flash::success("Se ha registrado de manera exitosa!")->important();
        return redirect()->route('ventas.cliente.index');
    }

    public function show($id)
    {
        $cliente = Cliente::findOrFail($id);
        return view('ventas.cliente.show', compact('cliente'));
    }

    public function edit($id)
    {
        $tipo_docs = Tipo_docs::orderBy('nombre', 'ASC')->lists('nombre', 'idtipo_doc');

        $rscliente = Cliente::find($id);
        return view('ventas.cliente.edit', compact('rscliente', 'tipo_docs' ));
    }

    public function update(Request $request, Cliente $cliente)
    {
        $request = $request->all();

       // $request['LAST_UPDATE_DATE'] = Carbon::now();
        $request['last_updated_by'] = Auth()->user()->id;

        $request['full_name'] = $request['first_last_name'] . " " . $request['second_last_name'] . " " . $request['first_name'] . " " . $request['second_name'];

        $cliente->update($request);

        Flash::success("El cliente ha sido editado con exito!")->important();

        return redirect()->route('ventas.cliente.index');
    }

    public function exportCliente(Request $request, Cliente $cliente)
    {
        Excel::create('Lista de Clientes', function ($excel) {

            $excel->sheet('Listado', function ($sheet) {

                $date = Carbon::now()->format('Y-m-d');
                $cliente = Cliente::orderBy('first_last_name', 'ASC')->where('effective_end_date', '>=', $date)->get();

                $sheet->fromArray($cliente);

            });
        })->export('xls');
    }

    //uso FACTURA/RECIBOS
    public function showApi(Request $request)
    {

    	if ($request->mount == '1') {
                	$factura = Factura::find($request->customer_trx_id);
                	$shipTo =  Cliente::find($factura->ship_to_customer_id)->toArray();
                	$billTo = Cliente::find($factura->bill_to_customer_id)->toArray();

                	$clients = [
                		'shipTo' => $shipTo,
                		'billTo' => $billTo,
    	];

    	}
    	else if(isset($request->idcliente))
            {
    		$clients =  Cliente::find($request->idcliente)->toArray();
    	}
            else
            {
                $clients = Cliente::all()->toArray();
            }

            return  $clients;
    }

    public function pendingBills($id)
    {
        $bills = Factura::where('bill_to_customer_id', $id)
            ->with('terms')
            ->with('billCurrency')
            ->where('status_trx', '<>', 'CANCEL')
            ->where('complete_flag', 'Y')
            ->get();

        foreach ($bills as $key => $bill) {
            if ($bill->balance> 0) {
                $bill['balance'] = $bill->balance;
            }

            if (count($bill->terms) > 0){
                foreach ($bill->terms as $term) {
                $term['balance'] = $term->balance;
                }
            }
        }
        return $bills;
    }
    //FIN - uso FACTURA/RECIBOS


    //view information history record
    public function showDataModal($id)
    {
        $data = Cliente::where('idcliente', $id)->first();
        $creator = \sisVentas\User::where('id', $data->created_by)->first();
        $updator = \sisVentas\User::where('id', $data->last_updated_by)->first();

        return view('modal.modal_body', compact('data', 'creator', 'updator' ));
    }

    public function storeClienteApi(Request $request)
    {
        $request = $request->all();

        $request['created_by'] = Auth()->user()->id;
        $request['effective_end_date'] ='01-01-2099';
        $request['last_updated_by'] = Auth()->user()->id;

        $request['full_name'] = $request['first_last_name']  . " " . $request['first_name'] ;

        // dd( $request);
        $cliente = new Cliente($request);
        $cliente->save();

        return response()->json(['cliente' => $cliente]);
    }

}
