<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\WfNotifications;
use sisVentas\Http\Requests;
use sisVentas\FsetupReminder;
use Laracasts\Flash\Flash;
use Mail;
use Auth;
use Session;
use sisVentas\Site;

class MailController extends Controller
{		
		public function notifications()
		{
			$lista = WfNotifications::get();
			return view('emails.notifications')->with('listado', $lista);
		}
    
	     public function enviarmail(Request $req)
	     {
                 $data = $req->all();
                 $user = FsetupReminder::all();

                 
                 $separador = ","; 
                 $mi_cadena = $user[0]->cc;

                 $array = explode($separador,$mi_cadena); 
             
           $correo = $req->input('x');
           $c1 = $user[0]->correode;
          // return $c1;

            Mail::send('email.message', ['user' => $user], function ($m) use ($user, $correo, $array) {
                $m->from($user[0]->correode, 'Your Application');
    
                $m->to($correo)->cc($array)->subject($user[0]->asunto);
                
            });
            
            

         }


    public function sendMailToVendor(Request $req)
     {
             $data = $req->all();

            dd($data);
             $user = FsetupReminder::all();
             $name = "recepcion_" .$recepcionid.".pdf";
             $ruta = asset("uploads/public/pdf/". $name); //"storage/public/pdf/recepcion_" 
             
             $separador = ","; 
             $mi_cadena = $user[0]->cc;

             $array = explode($separador,$mi_cadena); 
         
       $correo = $req->input('x');
       $c1 = $user[0]->correode;
      // return $c1;

        Mail::send('email.message', ['user' => $user], function ($m) use ($user, $correo, $array) {
            $m->from($user[0]->correode, 'Your Application');

            $m->to($correo); //->cc($array)
            $m->subject($user[0]->asunto);
            $m->attach($ruta, [
                'as' => $name,
                'mime' => 'application/pdf'
                ]);
            
        });
         

     }

         public function create()
         {

            $sucursales = Site::select('name', 'id')->where('condicion','=', 1)->orderBy('name', 'ASC')->get();

          //  dd($sucursales);
            return view('configuracion.correos.create')->with('sucursales',$sucursales);
         }
         

         public function store2(Request $r)
         {
                dd($r);
             $savemail = new FsetupReminder();
             $savemail->mailto = $r->input('mailto');
             $savemail->cc = $r->input('cc');
             $savemail->message = $r->input('message');
             $savemail->description_use = $r->input('description_use');
             $savemail->module = $r->input('module');
             $savemail->subject = $r->input('subject');
             $savemail->due_days = $r->input('due_days');
            // $savemail->site_id = Session::get('site_id'); 
             $savemail->created_by = Auth::user()->id;
             $savemail->last_updated_by = Auth::user()->id;

             //return $savemail;

             $savemail->save();

             Flash::success("Se ha registrado de manera exitosa!")->important();

             return redirect()->route('configuracion.correo.index');


         }

         public function indextest()
         {
             $data = FsetupReminder::all();

            return view('configuracion.correostest.index', compact('data'));
         }

         public function index()
         {
             $data = FsetupReminder::all();

            return view('configuracion.correos.index', compact('data'));
         }

         public function edit($id)
         {
              $edit = FsetupReminder::find($id);
               $sucursales = Site::select('name', 'id')->where('condicion','=', 1)->orderBy('name', 'ASC')->get();

               //dd( $edit);
              return view('configuracion.correos.edit', compact('edit','sucursales'));
 
         }

          public function edittest($id)
         {
              $edit = FsetupReminder::find($id);
               
              return view('configuracion.correostest.edit', compact('edit' ));
 
         }

         public function update(Request $r, $id)
         {
               // dd($r);
             $savemail = FsetupReminder::find($id);
             $savemail->mailto = $r->input('mailto');
             $savemail->cc = $r->input('cc');
             $savemail->message = $r->input('message');
             $savemail->description_use = $r->input('description_use');
             $savemail->module = $r->input('module');
             $savemail->subject = $r->input('subject');
             $savemail->due_days = $r->input('due_days');
            
             $savemail->last_updated_by = Auth::user()->id;

          // dd( $savemail);

            //return $savemail;

            $savemail->save();

            Flash::success("Se ha actualizado de manera exitosa!")->important();

            return redirect()->route('configuracion.correo.index');
         }



}
