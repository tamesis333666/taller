<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\User;

use sisVentas\Cliente;

use Auth;

use Laracasts\Flash\Flash;

class UserWebController extends Controller
{
    //
    public function index()
    {   
        $rssite = User::where('type', 'Externo')->get();
        return view('webfront.index', compact('rssite'));
    }

    public function create()
    {
        $client = Cliente::pluck('full_name','idcliente');
        return view('webfront.create', compact('client'));
    }

    public function store(Request $request)
    {
       // return $request->all();

        $usuarioweb = new User();

            $usuarioweb->name = $request->get('name');
            $usuarioweb->person_id = $request->get('person_id');
            $usuarioweb->effective_end_date = $request->get('effective_end_date');
            $usuarioweb->email = $request->get('email');
            $usuarioweb->type = $request->get('type');
            $usuarioweb->password = bcrypt($request->get('password'));
            $usuarioweb->created_by = Auth::user()->id;
            $usuarioweb->last_updated_by = Auth::user()->id;

            $usuarioweb->save();

            Flash::success('Se ha registrado el usuario correctamente');

            
            return redirect('/usuario/web');
            


    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $rssite = User::find($id);
        $client = Cliente::pluck('full_name','idcliente');

        return view('webfront.edit', compact('rssite','client'));

    }
 

    public function update(Request $request, $id)
    {

        $usuarioweb =  User::find($id);

        $usuarioweb->name = $request->get('name');
        $usuarioweb->person_id = $request->get('person_id');
        $usuarioweb->effective_end_date = $request->get('effective_end_date');
        $usuarioweb->email = $request->get('email');
        $usuarioweb->type = $request->get('type');
        $usuarioweb->password = bcrypt($request->get('password'));
       
        $usuarioweb->last_updated_by = Auth::user()->id;

        $usuarioweb->save();

        Flash::success('Se ha actualizado el usuario correctamente');

        
        return redirect('/usuario/web');

    }
}
