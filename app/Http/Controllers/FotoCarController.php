<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\User;

use sisVentas\Cliente;

use sisVentas\AsCotizacion;

use sisVentas\FndAttachedDocuments;
use Auth;
use sisVentas\Appointment;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;


class FotoCarController extends Controller
{
    //
    public function index()
    {   

      return view('appointment.receipt.upload-page');
    }

    public function create(Request $request)


    {

         return view('appointment.receipt.upload-page');
    }

    public function store(Request $request)
    {
       // return $request->all();

       

           
            


    }

    public function show($id)
    {
      return view('appointment.receipt.SeleccionaCamara',compact('id'));
    }

    public function edit(Request $request,$id)

    {
      $camara = $request->get('camara');

      $citas  = DB::table('mkt_reservations')
     ->join('cliente','cliente.idcliente','=','mkt_reservations.customer_id')
     ->join('vehiculo','vehiculo.id','=','mkt_reservations.placa_id')
    
      ->select('mkt_reservations.id as num_cita','cliente.idcliente','cliente.full_name','vehiculo.id','vehiculo.placa')
      ->groupBy('mkt_reservations.id')
      ->where('mkt_reservations.id','=',$id)
      ->get();

      
      if($camara == "frontal"){
          return view('appointment.receipt.generaCita',compact('citas'));
        }else{
           return view('appointment.receipt.generaCitaMovil',compact('citas'));
        }

        

       
       

    }
 

    public function update(Request $request, $id)
    {

        

    }
    public function subir(Request $request,$id,$placa, $cliente){
      

       try{
            DB::beginTransaction();
          

              $actualizar=Appointment::where('id',$id)
              ->update([
                  "status"=>"Recepcion",
                
              ]);

              
            

              $valor =$id;


              $destino = "destino/".$valor."/";
              if (!file_exists($destino)) {
                  mkdir($destino, 0777, true);
              }

              // $i=0;
              // echo print_r($_POST);
              // echo $_POST["filesToUpload_".($i+2)];

              for($i=1; $i < count($_POST); $i++){
              echo $_POST["filesToUpload_".($i)];

                 $base_to_php = explode(',', $_POST["filesToUpload_".($i)]);


                  $data = base64_decode($base_to_php[1]);

                 file_put_contents($destino."imagen".$i.".png", $data);
                 echo $destino."imagen".$i.".png Creada <br/>";


              }

              //TABLA FND ATTACHED DOCUMENT
                for($i=1; $i < count($_POST); $i++){
              $documento = new FndAttachedDocuments();
              $documento->entity_name  = "valor fijo = Receipt Appoit";
              $documento->pk1_value  = $valor;
              $documento->name_file   = "imagen".$i.".png";
              $documento->path_file   = $destino."imagen".$i.".png";
              $documento->created_by  =  Auth::user()->id;
              $documento->last_updated_by = Auth::user()->id;
               $documento->save();
               }

               //INGRESO A LA TABLA DE COTIZACION
               $cotizacion = new AsCotizacion();

               $cotizacion->nrocotizac =0;
               $cotizacion->site_id =0;
               $cotizacion->Idcliente =$cliente;
               $cotizacion->id_use_vehicle =0;
               $cotizacion->placa =$placa;
               $cotizacion->idasesor =0;
               $cotizacion->comments ="prueba";
               $cotizacion->dscto =0.0;
               $cotizacion->totalrep =0.0;
               $cotizacion->total =0.0;
               $cotizacion->appoinment_id =$id;
               $cotizacion->totalmo =0.0;
               $cotizacion->subtotal =0.0;
               $cotizacion->igv =0.0;
               $cotizacion->tipocot ="";
               $cotizacion->last_updated_by =Auth::user()->id;
               $cotizacion->created_by =Auth::user()->id;
               $cotizacion->save();



            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
        }
     

     



         return view('appointment.receipt.upload-page',compact('id','valor'));
    }
}
