<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;
use sisVentas\Http\Requests\VehiculoFormRequest;
use sisVentas\Vehiculo;
use sisVentas\Appointment;
use sisVentas\User;
use sisVentas\createby;
use sisVentas\FndLookup;
use Illuminate\Support\Facades\Session; 
use sisVentas\Site;
use sisVentas\Labor;
use Carbon\Carbon;
use sisVentas\Cliente;
use sisVentas\FndAttachedDocuments; 
use Laracasts\Flash\Flash;
use sisVentas\Http\Requests;
 
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Illuminate\Support\Facades\Auth;

class AppointmentController extends Controller
{


        public function receipt_cita()

    {     
         $datos = Appointment::where('status','<>','Anulado')->with('cliente')->orderBy('id', 'DESC')->get();
        return view('appointment.receipt.index')->with('datos', $datos);

    }


        public function cita_cliente($cita_id)

    {     
        $d = Appointment::where('id',$cita_id)->first();
  
        return \PDF::loadView('appointment.register.pdfs.appointmentCust', compact('d'))->setpaper('a4')->stream(); 

    }

  //historial de cotizaciones
   public function historial()

    {   
        
        $labores = labor::select('nombrelabor', 'idlabor')->where('condicion','=',1)->orderBy('nombrelabor', 'ASC')->get(); 

        $vehiculos = Vehiculo::select('placa', 'id')->where('no_atender','=',0)->orderBy('placa', 'ASC')->get(); 
        $clientes = Cliente::select('full_name', 'idcliente')->where('no_atender','=', 0)->orderBy('full_name', 'ASC')->get();
        $sucursales = Site::select('name', 'id')->where('condicion','=', 1)->orderBy('name', 'ASC')->get();

  
        return view('appointment.register.filters')->with('labores',$labores)
                                            ->with('vehiculos',$vehiculos)
                                            ->with('clientes',$clientes)
                                            ->with('sucursales',$sucursales)
                                            ;

    }

    public function export(Request $request)
    {   
        $mytime = Carbon::now()->format('d.m.Y H-i');
        $varname =  $mytime;
        $pfilename = 'Parte de Servicios_' .$varname ;

        Excel::create($pfilename, function($excel) {
            $excel->sheet('listado', function($sheet) {
                 $vehiculos = PartService::orderBy('id', 'desc')->get();
                $sheet->loadView('mortuary.service.excel.export')->with('arrdatos', $vehiculos);
            });
        })->export('xls');
    }

    public function index()
    {
        $datos = Appointment::with('cliente')->orderBy('id', 'DESC')->get();
        return view('appointment.register.index')->with('datos', $datos);
    }


    public function create()
    {
        $date = Carbon::now()->format('Y-m-d');

        $operacion = 'Create';

        $labores = Labor::select('nombrelabor', 'idlabor')->where('condicion','=',1)->orderBy('nombrelabor', 'ASC')->get(); 

        $vehiculos = Vehiculo::select('placa', 'id')->where('no_atender','=',0)->orderBy('placa', 'ASC')->get(); 
        $clientes = Cliente::select('full_name', 'idcliente')->where('no_atender','=', 0)->orderBy('full_name', 'ASC')->get();
        $sucursales = Site::select('name', 'id')->where('condicion','=', 1)->orderBy('name', 'ASC')->get();

        
        return view('appointment.register.create')->with('sucursales', $sucursales)->with('clientes', $clientes)->with('vehiculos', $vehiculos)->with('labores', $labores)->with('operacion', $operacion);
    }
    
    /**
     * Function to save PartService
     */
    public function save_service(Request $request) 
    {
        
        if ($request->action == 'Nuevo') {
            $service = new Appointment();

        } else {
            $service = Appointment::findOrFail($request->id);
        }
        
      
        $service->site_id =  $request->site_id;
        $service->placa_id = $request->placa_id;
        $service->status = "Nuevo";
         
        $service->customer_id = $request->customer_id;
        $service->datetime_reservation = $request->datetime_reservation;

        $service->service1_id = $request->service1_id;
        $service->service2_id = $request->service2_id;
        $service->comments = $request->comments;
        
        $service->confirmed = 'N';
        
        $service->created_by =  Auth()->user()->id;
        $service->last_updated_by =  Auth()->user()->id;
        
     //   dd( $service);

        if ($service->save()) {
           if ($request->action == 'Nuevo') {
                Flash::success("Se ha registrado de manera exitosa!")->important();
           } 
           else 
            {   
                //INICIO ATTACH DOCUMENT 
                if ($request->hasFile('file')) 
                {
                    $idrecord = $service->id;
                    $numrecod = $service->numpart; 
                    $entity_name = 'Mort_PartService';
                    //dd($purchaseOrder->site_id)  ;
                  $files = $request->file('file');
                 // dd($files);
                  foreach($files as $file) {
                    $documento = $service->site_id."_".$numrecod."_".$file->getClientOriginalName();
                    $destination = base_path() . '/public/documents/PartService';
                    try {
                      $file->move($destination, $documento);
                    } catch (\Exception $e) {
                      // dd('paso1');
                      DB::rollback();
                    }
                    try {
                      $saveFile = FndAttachedDocuments::create([
                                                                'pk1_value' => $idrecord,
                                                                'path_file' => $destination.'/'.$documento,
                                                                'description' => $request->description_adjunto,
                                                                'entity_name' => $entity_name ,
                                                                'name_file' => $documento,
                                                                'created_by' => Auth::user()->id,
                                                                'last_updated_by' => Auth::user()->id,
                                                              ]);
                    } catch (\Exception $e) {
                      // dd($e);
                      DB::rollback();
                    }
                  }//fin foreach
                } //fin if
               

                DB::commit();
                //FIN ATTACHD DOCUMENT        
                    

                Flash::success("Se ha actualizado de manera exitosa!")->important();
            }           
           return redirect('appointment/servicio');//->route('mortuary.service.index');
        }
    }

    /**
     * Function to view edit
     */
    public function edit($id) {
        $service = PartService::findOrFail($id);
        if ($service) {
            $date = Carbon::now()->format('Y-m-d');
            $tipomov =  FndLookup::where('lookup_type', 'MOVILIDAD_FUNERAL')->first();
            $tipomov = (isset($tipomov) ? $tipomov->fndLookupValue->pluck('description', 'idlvalue')->toArray() : []);
    
            $tipodes =  FndLookup::where('lookup_type', 'TIPO_DESPACHO_FUNERARIA')->first();
            $tipodes = (isset($tipodes) ? $tipodes->fndLookupValue->pluck('description', 'idlvalue')->toArray() : []);

            $capilla =  FndLookup::where('lookup_type', 'CAPILLA_FUNERAL')->first();
                
            $capilla = (isset($capilla) ? $capilla->fndLookupValue->pluck('description', 'idlvalue')->toArray() : []); 

    
            $modelocap =  FndLookup::where('lookup_type', 'MODELO_CAPILLA_FUNERAL')->first();
            $modelocap = (isset($modelocap) ? $modelocap->fndLookupValue->pluck('description', 'idlvalue')->toArray() : []);        
           
            $clientes = Cliente::orderBy('full_name', 'ASC')->where('effective_end_date', '>=', $date)->lists('full_name', 'idcliente');

            $cementerio = Cementerio::orderBy('nombre', 'ASC')->where('condicion', '=', 1)->lists('nombre', 'id');

            $operacion = 'Edit';
            
            return view('mortuary.service.edit')->with('service', $service)->with('clientes', $clientes)->with('cementerio', $cementerio)->with('capilla', $capilla)->with('modelocap', $modelocap)->with('operacion', $operacion)->with('tipodes', $tipodes)->with('tipomov', $tipomov);;
        }
    }

    /**
     * Function to delete service
     * @param {id} Id del renglon
     */
    public function delete_service(Request $request) {
        $id = $request->id;
        $service = Appointment::findOrFail($id);
        $result = array();
        if ($service) {
            $service->status = "Anulado";
            $service->updated_at = Carbon::now();
            $service->last_updated_by = Auth()->user()->id;
            if ($service->save()) {
                //Flash::success("Se ha anulado el registro de manera exitosa!")->important();
                //return redirect('funeraria/servicio');
                $result = array(
                    "status" => 1,
                    "type" => "success",
                    "message" => "Se ha anulado el registro de manera exitosa!"
                );
            } else {
                $result = array(
                    "status" => 0,
                    "type" => "error",
                    "message" => "No se pudo anular el registro, intenta mas tarde!"
                );
            }
        } else {
            $result = array(
                "status" => 0,
                "type" => "warning",
                "message" => "No se encontro el registro a anular, favor de verificar!"
            );
        }
        return response()->json($result);
    }


    public function store(VehiculoFormRequest $request)
    {
        $request = $request->all();

        $request['created_by'] = Auth()->user()->id;
        $request['last_updated_by'] = Auth()->user()->id;

        $dt = Carbon::create($request['año']);
        $dt->format('Y');
        $dt->startOfYear();
        $request['año'] = $dt;

        $vehiculos = new Vehiculo($request);
        $vehiculos->save();
        $vehiculos->manyCombustions()->sync($request['combustions']);

        Flash::success("Se ha registrado de manera exitosa!")->important();

        return redirect()->route('asesor.vehiculo.index');
    }
 

 

   
}
