<?php
 
namespace sisVentas;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class vEamOrders extends Model
{
    protected $table = "v_lista_pedidos_despachar";
     

    protected $fillable = ['wip_entity_id','wip_entity_name','full_name'
    						,'placa',
                            'fecha_termino',
                            'fecha_inicio'                             
							 ];
 
}
