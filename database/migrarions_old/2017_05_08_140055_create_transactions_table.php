<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_material_transactions', function (Blueprint $table) {
            $table->increments('transaction_id');
            $table->unsignedInteger('transaction_set_id')->nullable();
            $table->unsignedInteger('item_id');
            $table->unsignedInteger('subinventory_id');
            $table->unsignedInteger('locator_id');
            $table->unsignedInteger('transaction_type_id');
            $table->integer('transaction_quantity');
            $table->string('primary_uom_code');
            $table->datetime('transaction_date');
            $table->unsignedInteger('type_doc_id');
            $table->string('num_doc');
            $table->string('ref_doc');
            $table->string('obs_doc');
            $table->unsignedInteger('last_updated_by');
            $table->unsignedInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inv_material_transactions');
    }
}
