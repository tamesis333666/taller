<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlazoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plazos', function(Blueprint $table)
        {
            $table->increments('id' ,true);
            $table->integer('customer_trx_id');
            $table->integer('term');
            $table->string('status')->default('No Pagado');
            $table->dateTime('due_date');
            $table->decimal('amount');
            $table->integer('receipt_id')->nullable();
            $table->dateTime('pay_date')->nullable();
            $table->string('comment')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plazos');
        //
    }
}
