<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fnd_login', function (Blueprint $table) {
             $table->increments('IDLOGUEO');
             $table->integer('IDUSUARIO');
             $table->dateTime('HRAINICIO');
             $table->dateTime('HRAFIN')->nullable();
             $table->string('IDTERMINAL');
             $table->integer('created_by');
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fnd_login');
    }
}
