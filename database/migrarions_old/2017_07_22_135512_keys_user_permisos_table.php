<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KeysUserPermisosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_permisos', function (Blueprint $table) {

            $table->foreign('id_user')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('users_permisos'))
        {
            Schema::table('users_permisos', function (Blueprint $table) {
                $table->dropForeign('stock_id_article_foreign');
                $table->dropForeign('stock_id_branche_foreign');
            });
        }
    }
}
