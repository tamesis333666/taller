<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fnd_usersite', function (Blueprint $table) {
            $table->increments('IDUSERSITE');
            $table->integer('IDUSUARIO');
            $table->integer('IDSITE');
            $table->dateTime('FEC_INI');
            $table->dateTime('FEC_FIN');
            $table->integer('ULTACTPOR');
            $table->integer('CREPOR');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fnd_usersite');
    }
}
