<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fnd_warehouse', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('site_id')->unsigned();
            $table->foreign('site_id')->references('id')->on('fnd_site');
            $table->timestamps();
        });   
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fnd_warehouse');
    }
}
