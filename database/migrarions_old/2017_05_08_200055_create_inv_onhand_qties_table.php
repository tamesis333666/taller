<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvOnhandQtiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_onhand_quantities_detail', function (Blueprint $table) {
            $table->increments('onhand_quantities_id');
            $table->unsignedInteger('item_id');
            $table->unsignedInteger('organization_id')->nullable();
            $table->dateTime('date_received');
            $table->unsignedInteger('last_updated_by');
            $table->unsignedInteger('created_by');
            $table->integer('primary_transaction_quantity');
            $table->unsignedInteger('subinventory_id');
            $table->unsignedInteger('locator_id');
            $table->unsignedInteger('create_transaction_id');
            $table->dateTime('orig_date_received');
            $table->string('transaction_uom_code');
            $table->integer('transaction_quantity');
            $table->boolean('is_consigned');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inv_onhand_quantities_detail');
    }
}
