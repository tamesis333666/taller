<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvSubInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_sub_inventory', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('site_id')->unsigned();
            $table->foreign('site_id')->references('id')->on('inv_sites');
            $table->timestamps();
        });   
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inv_sub_inventory');
    }
}
